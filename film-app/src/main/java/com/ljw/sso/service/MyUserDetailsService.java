package com.ljw.sso.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ljw.sso.info.AuthorityInfo;
import com.ljw.sso.info.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.cas.authentication.CasAssertionAuthenticationToken;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 用于加载用户信息 实现UserDetailsService接口，或者实现AuthenticationUserDetailsService接口
 * @author ChengLi
 *
 */
public class MyUserDetailsService /*
    //实现UserDetailsService接口，实现loadUserByUsername方法
    implements UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("当前的用户名是："+username);
        //这里我为了方便，就直接返回一个用户信息，实际当中这里修改为查询数据库或者调用服务什么的来获取用户信息
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("admin");
        userInfo.setName("admin");
        Set<AuthorityInfo> authorities = new HashSet<AuthorityInfo>();
        AuthorityInfo authorityInfo = new AuthorityInfo("TEST");
        authorities.add(authorityInfo);
        userInfo.setAuthorities(authorities);
        return userInfo;
    }*/
        //实现AuthenticationUserDetailsService，实现loadUserDetails方法
        implements AuthenticationUserDetailsService<CasAssertionAuthenticationToken> {

    @Autowired
    RestTemplate restTemplate;

    @Value("${user.role.authority.url}")
    private String authorityUrl;

    @Override
    public UserDetails loadUserDetails(CasAssertionAuthenticationToken token) throws UsernameNotFoundException {
         /*这里调用服务来获取用户信息*/
        String userId = token.getName();
        String userString =restTemplate.getForEntity(authorityUrl+"/user/getById/"+userId, String.class).getBody();
        //String userString = HttpUtil.get(authorityUrl+"/user/getById/"+userId);
        JSONObject json = JSON.parseObject(userString);
        Map<String,Object> userMap = json.getJSONObject("data");
        UserInfo userInfo = new UserInfo();
        userInfo.setId(userMap.get("id").toString().trim());
        userInfo.setUsername(userMap.get("account").toString().trim());
        userInfo.setName(userMap.get("uname").toString().trim());
        userInfo.setPassword(userMap.get("password").toString().trim());

        //获取用户角色
         json.clear();
         json.put("account",userInfo.getUsername());
         String userRoleString = restTemplate.postForEntity(authorityUrl+"/userRole/getAll",json, String.class).getBody();
         //String userRoleString = HttpUtil.post(authorityUrl+"/userRole/getAll",json.toJSONString());
         json = JSON.parseObject(userRoleString);
         JSONArray userRoleList = json.getJSONArray("data");
         String userRoleIds = "";
        for ( int i = 0; i< userRoleList.size();i++){
            json = (JSONObject) userRoleList.get(i);
            userRoleIds = json.get("roleId")+","+userRoleIds;
        }

        //获取用户角色权限
        json.clear();
        json.put("roleIds",userRoleIds);
        String permission = restTemplate.postForEntity(authorityUrl+"/permission/getAll",json, String.class).getBody();
        //String permission = HttpUtil.post(authorityUrl+"/permission/getAll",json.toJSONString());
        json = JSON.parseObject(permission);
        JSONArray permissionList = json.getJSONArray("data");

        //将权限加入权限框架
        Set<AuthorityInfo> authorities = new HashSet<AuthorityInfo>();
        AuthorityInfo authorityInfo = null;
        for ( int j = 0; j< permissionList.size();j++){
            json = (JSONObject) permissionList.get(j);
            authorityInfo = new AuthorityInfo(json.get("perKey").toString().trim());
            authorities.add(authorityInfo);
        }
        userInfo.setAuthorities(authorities);
        return userInfo;
    }

}
