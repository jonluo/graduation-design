package com.ljw.utils;

import java.util.UUID;

/**
 * Created by 钜文 on 2017/2/3.
 */
public class UuidUtils {

    public static String getUUID(String prefix){
        return prefix+"0000"+UUID.randomUUID().toString().replace("-","");
    }
}
