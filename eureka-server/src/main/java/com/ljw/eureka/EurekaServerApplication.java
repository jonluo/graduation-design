package com.ljw.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author 罗钜文
 * spring cloud eureka
 */

@SpringBootApplication
@EnableEurekaServer //开启eureka服务端
public class EurekaServerApplication {
	public static void main(String[] args) {
        SpringApplication.run(EurekaServerApplication.class, args);
    }
}
