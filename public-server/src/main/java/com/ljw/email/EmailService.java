package com.ljw.email;


import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by 钜文 on 2017/1/25.
 */
@RestController
@RequestMapping(value="/email")
public class EmailService {

    @Autowired
    private JavaMailSender mailSender;
    @Value("${spring.mail.username}")
    private String username;

    /**
     * @param to 发送的邮件地址
     * @param subject 主题
     * @param text 内容
     * @param fileName 附件名称 注意：文件要放在resource里
     * @throws Exception
     */
    private void sendMail(String to, String subject, String text, String fileName) throws Exception {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setFrom(username);
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(text);
        if (fileName!=null && !fileName.equals("")) {
            ClassPathResource file = new ClassPathResource(fileName);
            helper.addAttachment(fileName, file);//附件
        }
        mailSender.send(mimeMessage);
    }


    @ApiOperation(value="发送邮件", notes="发送认证邮件",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"to\":\"i@.com\",\"text\":\"233\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/sent"}, method= RequestMethod.POST)
    public ResponseEntity sent(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            String to = jsonObject.getString("to");
            String text = jsonObject.getString("text");
            sendMail(to,"找回密码",text,null);
            map.put("code", "0");
            map.put("msg", "发送邮件成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("msg", "发送邮件失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }


}
