package com.ljw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * springboot 入口
 */
@SpringBootApplication
@EnableCaching         //开启缓存
//@EnableScheduling      //开启定时任务
//@EnableAsync           //异步调用
@EnableDiscoveryClient  //服务注册和发现
public class PublicServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PublicServerApplication.class, args);
	}

}
