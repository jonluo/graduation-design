package com.ljw.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.Future;

/**
 * Created by 钜文 on 2017/1/24.
 * 定时任务+异步调用
 */
@Component
public class ScheduledTasks {

    @Autowired
    private Task task;

   /* @Scheduled(cron = "* *//*10 * * * *")*/
    public void reportTasksTime() {
        long start = System.currentTimeMillis();
      try {
          Future<String> task1 = task.doTaskOne();
          Future<String> task2 = task.doTaskTwo();
          Future<String> task3 = task.doTaskThree();
          while(true) {
              if(task1.isDone() && task2.isDone() && task3.isDone()) {
                  // 三个任务都调用完成，退出循环等待
                  break;
              }
              Thread.sleep(1000);
          }
      }catch (Exception e){
          System.out.println(e.getMessage());
      }
        long end = System.currentTimeMillis();
        System.out.println("任务全部完成，总耗时：" + (end - start) + "毫秒");
    }
}
