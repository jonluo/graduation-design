package com.ljw.mapper;

import com.ljw.model.Application;
import com.ljw.model.ApplicationExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;
@Mapper
public interface ApplicationMapper {
    @Cacheable(value="applicationCount",key="'applicationCount'+#p0.toString()")
    int countByExample(ApplicationExample example);

    int deleteByExample(ApplicationExample example);
    @Caching(evict = { @CacheEvict(value={"applicationList","applicationCount"},allEntries=true),
            @CacheEvict(value="application",key="#p0")})
    int deleteByPrimaryKey(String id);

    int insert(Application record);
    @Caching(evict = { @CacheEvict(value={"applicationList","applicationCount"},allEntries=true),
            @CacheEvict(value="application",key="#p0.id")})
    int insertSelective(Application record);
    @Cacheable(value="applicationList",key="'applicationList'+#p0.toString()")
    List<Application> selectByExample(ApplicationExample example);
    @Cacheable(value="application",key="#p0")
    Application selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") Application record, @Param("example") ApplicationExample example);

    int updateByExample(@Param("record") Application record, @Param("example") ApplicationExample example);
    @Caching(evict = { @CacheEvict(value="applicationList",allEntries=true),
            @CacheEvict(value="application",key="#p0.id")})
    int updateByPrimaryKeySelective(Application record);

    int updateByPrimaryKey(Application record);
}