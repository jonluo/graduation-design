package com.ljw.mapper;

import com.ljw.model.UserRole;
import com.ljw.model.UserRoleExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;
@Mapper
public interface UserRoleMapper {
    @Cacheable(value="userRoleCount",key="'userRoleCount'+#p0.toString()")
    int countByExample(UserRoleExample example);

    int deleteByExample(UserRoleExample example);
    @Caching(evict = { @CacheEvict(value={"userRoleList","userRoleCount"},allEntries=true),
            @CacheEvict(value="userRole",key="#p0")})
    int deleteByPrimaryKey(Integer id);

    int insert(UserRole record);
    @Caching(evict = { @CacheEvict(value={"userRoleList","userRoleCount"},allEntries=true),
            @CacheEvict(value="userRole",key="#p0.id")})
    int insertSelective(UserRole record);
    @Cacheable(value="userRoleList",key="'userRoleList'+#p0.toString()")
    List<UserRole> selectByExample(UserRoleExample example);
    @Cacheable(value="userRole",key="#p0")
    UserRole selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") UserRole record, @Param("example") UserRoleExample example);

    int updateByExample(@Param("record") UserRole record, @Param("example") UserRoleExample example);
    @Caching(evict = { @CacheEvict(value="userRoleList",allEntries=true),
            @CacheEvict(value="userRole",key="#p0.id")})
    int updateByPrimaryKeySelective(UserRole record);

    int updateByPrimaryKey(UserRole record);
}