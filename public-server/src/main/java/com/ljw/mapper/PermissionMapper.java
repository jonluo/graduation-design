package com.ljw.mapper;

import com.ljw.model.Permission;
import com.ljw.model.PermissionExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;
@Mapper
public interface PermissionMapper {
    @Cacheable(value="permissionCount",key="'permissionCount'+#p0.toString()")
    int countByExample(PermissionExample example);

    int deleteByExample(PermissionExample example);
    @Caching(evict = { @CacheEvict(value={"permissionList","permissionCount"},allEntries=true),
            @CacheEvict(value="permission",key="#p0")})
    int deleteByPrimaryKey(Integer id);

    int insert(Permission record);
    @Caching(evict = { @CacheEvict(value={"permissionList","permissionCount"},allEntries=true),
            @CacheEvict(value="permission",key="#p0.id")})
    int insertSelective(Permission record);
    @Cacheable(value="permissionList",key="'permissionList'+#p0.toString()")
    List<Permission> selectByExample(PermissionExample example);
    @Cacheable(value="permission",key="#p0")
    Permission selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Permission record, @Param("example") PermissionExample example);

    int updateByExample(@Param("record") Permission record, @Param("example") PermissionExample example);
    @Caching(evict = { @CacheEvict(value="permissionList",allEntries=true),
            @CacheEvict(value="permission",key="#p0.id")})
    int updateByPrimaryKeySelective(Permission record);

    int updateByPrimaryKey(Permission record);
}