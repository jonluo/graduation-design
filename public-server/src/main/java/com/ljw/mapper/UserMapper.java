package com.ljw.mapper;

import com.ljw.model.User;
import com.ljw.model.UserExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;
@Mapper
public interface UserMapper {
    @Cacheable(value="userCount",key="'userCount'+#p0.toString()")
    int countByExample(UserExample example);

    int deleteByExample(UserExample example);
    @Caching(evict = { @CacheEvict(value={"userList","userCount"},allEntries=true),
                       @CacheEvict(value="user",key="#p0")})
    int deleteByPrimaryKey(String id);

    int insert(User record);
    @Caching(evict = { @CacheEvict(value={"userList","userCount"},allEntries=true),
                       @CacheEvict(value="user",key="#p0.id")})
    int insertSelective(User record);
    @Cacheable(value="userList",key="'userList'+#p0.toString()")
    List<User> selectByExample(UserExample example);
    @Cacheable(value="user",key="#p0")
    User selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

    int updateByExample(@Param("record") User record, @Param("example") UserExample example);
    @Caching(evict = { @CacheEvict(value="userList",allEntries=true),
                       @CacheEvict(value="user",key="#p0.id")})
    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
}