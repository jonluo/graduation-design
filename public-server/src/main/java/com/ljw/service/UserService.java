package com.ljw.service;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ljw.mapper.UserMapper;
import com.ljw.model.User;
import com.ljw.model.UserExample;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 钜文 on 2017/1/21.
 */
@RestController
@Transactional
@RequestMapping(value="/user")
public class UserService {

    @Autowired
    private UserMapper userMapper;

    @ApiOperation(value="获取用户列表", notes="分页查询用户",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"start\":0,\"number\":\"10\",\"uname\":\"uname\"," +
            "\"tel\":\"tel1\",\"account\":\"account1\",\"password\":\"123\" }", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/getAll"}, method=RequestMethod.POST)
    public ResponseEntity getAll(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            Integer start = jsonObject.getInteger("start");
            Integer number = jsonObject.getInteger("number");
            String uname = jsonObject.getString("uname");
            String tel = jsonObject.getString("tel");
            String account = jsonObject.getString("account");
            String password = jsonObject.getString("password");

            UserExample userExample = new UserExample();
            UserExample.Criteria criteria= userExample.createCriteria();
            //分页
            userExample.setOffset(start);
            userExample.setLimit(number);
            //查询条件
            if (uname != null && !uname.equals("")) {
                criteria.andUnameLike("%" + uname + "%");
            }
            if (tel != null && !tel.equals("")) {
                criteria.andTelEqualTo(tel);
            }
            if (account != null && !account.equals("")) {
                criteria.andAccountEqualTo(account);
            }
            if (password != null && !password.equals("")) {
                criteria.andPasswordEqualTo( password);
            }

            List<User> list  = userMapper.selectByExample(userExample);
            Integer total = userMapper.countByExample(userExample);

            map.put("code", "0");
            map.put("total", total);
            map.put("data", list);
            map.put("msg", "查询成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("total",0);
            map.put("data", "");
            map.put("msg", "查询失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }


    @ApiOperation(value="获取用户", notes="根据账号查询用户",response = Map.class )
    @ApiImplicitParam(name = "account", value = "用户账号",paramType = "path",required = true, dataType = "String")
    @RequestMapping(value={"/validateLogon/{account}"}, method=RequestMethod.GET)
    public String validateLogon(@PathVariable String account ){
            UserExample userExample = new UserExample();
            UserExample.Criteria criteria= userExample.createCriteria();
            //查询条件
            if (account != null && !account.equals("")) {
                criteria.andAccountEqualTo(account);
            }
            List<User> list  = userMapper.selectByExample(userExample);
            User user = new User();
            if (list.size()>0){
                user = list.get(0);
             }
        //jsonp跨域请求
        return "jsonpcallback({\"id\":\""+user.getId()+"\",\"password\":\""+user.getPassword()+"\"})";
    }


    @ApiOperation(value="获取用户", notes="根据id查询用户",response = Map.class )
    @ApiImplicitParam(name = "id", value = "用户id",paramType = "path",required = true, dataType = "String")
    @RequestMapping(value={"/getById/{id}"}, method=RequestMethod.GET)
    public ResponseEntity getById(@PathVariable String id){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            User user = userMapper.selectByPrimaryKey(id);
            map.put("code", "0");
            map.put("data", user);
            map.put("msg", "查询成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("data", "");
            map.put("msg", "查询失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="添加用户", notes="添加用户信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"id\":\"id\",\"name\":\"name\",\"age\":1}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/add"}, method=RequestMethod.POST)
    public ResponseEntity add(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{

            User user = JSON.parseObject(jsonObject.toJSONString(), User.class);
            Integer sum = userMapper.insertSelective(user);
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "添加成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum",0);
            map.put("msg", "添加失败！"+e.getMessage());
        }
        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="修改用户", notes="根据id修改用户信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"id\":\"id\",\"name\":\"name\",\"age\":1}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/update"}, method=RequestMethod.POST)
    public ResponseEntity update(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            User user = JSON.parseObject(jsonObject.toJSONString(), User.class);
            Integer sum = userMapper.updateByPrimaryKeySelective(user);
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "修改成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum",0);
            map.put("msg", "修改失败！"+e.getMessage());
        }
        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="删除用户", notes="批量删除用户",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"ids\":\"id1,id2\"}多个id以逗号隔开", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/deleteByIds"}, method=RequestMethod.POST)
    public ResponseEntity deleteByIds(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            String idstring = jsonObject.getString("ids");
            String[] ids = idstring.split(",");
            Integer sum = 0;
            for (String id:ids){
                userMapper.deleteByPrimaryKey(id);
                sum++;
            }
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "批量删除成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum", 0);
            map.put("msg", "批量删除失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }


}
