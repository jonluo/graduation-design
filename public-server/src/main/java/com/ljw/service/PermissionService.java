package com.ljw.service;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ljw.mapper.PermissionMapper;
import com.ljw.model.Permission;
import com.ljw.model.PermissionExample;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 钜文 on 2017/1/21.
 */
@RestController
@Transactional
@RequestMapping(value="/permission")
public class PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;

    @ApiOperation(value="获取角色权限列表", notes="分页查询角色权限",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"start\":0,\"number\":\"10\",\"roleIds\":\"1,2\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/getAll"}, method=RequestMethod.POST)
    public ResponseEntity getAll(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            Integer start = jsonObject.getInteger("start");
            Integer number = jsonObject.getInteger("number");
            String roleIds = jsonObject.getString("roleIds");
            String permissionname = jsonObject.getString("permissionname");
            String perKey = jsonObject.getString("perKey");

            List<String> roleIdsList = roleIds == null ? null : Arrays.asList(roleIds.split(","));

            PermissionExample permissionExample = new PermissionExample();
            PermissionExample.Criteria criteria= permissionExample.createCriteria();
            //分页
            permissionExample.setOffset(start);
            permissionExample.setLimit(number);
            //查询条件
            if (roleIdsList != null && roleIdsList.size() > 0) {
                criteria.andRoleIdIn(roleIdsList);
            }
            if (permissionname != null && !"".equals(permissionname)) {
                criteria.andPermissionnameLike("%"+permissionname+"%");
            }
            if (perKey != null && !"".equals(perKey)) {
                criteria.andPerKeyEqualTo(perKey);
            }


            List<Permission> list  = permissionMapper.selectByExample(permissionExample);
            Integer total = permissionMapper.countByExample(permissionExample);

            map.put("code", "0");
            map.put("total", total);
            map.put("data", list);
            map.put("msg", "查询成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("total",0);
            map.put("data", "");
            map.put("msg", "查询失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }
    

    @ApiOperation(value="获取角色权限", notes="根据id查询角色权限",response = Map.class )
    @ApiImplicitParam(name = "id", value = "角色权限id",paramType = "path",required = true, dataType = "String")
    @RequestMapping(value={"/getById/{id}"}, method=RequestMethod.GET)
    public ResponseEntity getById(@PathVariable String id){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            Permission permission = permissionMapper.selectByPrimaryKey(Integer.parseInt(id));
            map.put("code", "0");
            map.put("data", permission);
            map.put("msg", "查询成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("data", "");
            map.put("msg", "查询失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="添加角色权限", notes="添加角色权限信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"id\":\"id\",\"permissionname\":\"name\",\"roleId\":\"1\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/add"}, method=RequestMethod.POST)
    public ResponseEntity add(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{

            Permission permission = JSON.parseObject(jsonObject.toJSONString(), Permission.class);
            Integer sum = permissionMapper.insertSelective(permission);
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "添加成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum",0);
            map.put("msg", "添加失败！"+e.getMessage());
        }
        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="修改角色权限", notes="根据id修改角色权限信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"id\":\"id\",\"permissionname\":\"name\",\"roleId\":\"22\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/update"}, method=RequestMethod.POST)
    public ResponseEntity update(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            Permission permission = JSON.parseObject(jsonObject.toJSONString(), Permission.class);
            Integer sum = permissionMapper.updateByPrimaryKeySelective(permission);
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "修改成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum",0);
            map.put("msg", "修改失败！"+e.getMessage());
        }
        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="删除角色权限", notes="批量删除角色权限",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"ids\":\"id1,id2\"}多个id以逗号隔开", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/deleteByIds"}, method=RequestMethod.POST)
    public ResponseEntity deleteByIds(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            String idstring = jsonObject.getString("ids");
            String[] ids = idstring.split(",");
            Integer sum = 0;
            for (String id:ids){
                permissionMapper.deleteByPrimaryKey(Integer.parseInt(id));
                sum++;
            }
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "批量删除成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum", 0);
            map.put("msg", "批量删除失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }


}
