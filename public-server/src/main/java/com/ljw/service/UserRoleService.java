package com.ljw.service;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ljw.mapper.UserRoleMapper;
import com.ljw.model.UserRole;
import com.ljw.model.UserRoleExample;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 钜文 on 2017/1/21.
 */
@RestController
@Transactional
@RequestMapping(value="/userRole")
public class UserRoleService {

    @Autowired
    private UserRoleMapper userRoleMapper;

    @ApiOperation(value="获取用户角色列表", notes="分页查询用户角色",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"start\":0,\"number\":\"10\",\"userId\":\"11\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/getAll"}, method=RequestMethod.POST)
    public ResponseEntity getAll(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            Integer start = jsonObject.getInteger("start");
            Integer number = jsonObject.getInteger("number");
            String account = jsonObject.getString("account");
            String roleName = jsonObject.getString("roleName");

            UserRoleExample userRoleExample = new UserRoleExample();
            UserRoleExample.Criteria criteria= userRoleExample.createCriteria();
            //分页

            userRoleExample.setOffset(start);
            userRoleExample.setLimit(number);
            //查询条件
            if (account != null && !account.equals("")) {
                criteria.andUserAccountEqualTo(account);
            }
            if (roleName != null && !roleName.equals("")) {
                criteria.andRoleNameLike("%"+roleName+"%");
            }

            List<UserRole> list  = userRoleMapper.selectByExample(userRoleExample);
            Integer total = userRoleMapper.countByExample(userRoleExample);

            map.put("code", "0");
            map.put("total", total);
            map.put("data", list);
            map.put("msg", "查询成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("total",0);
            map.put("data", "");
            map.put("msg", "查询失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }


    @ApiOperation(value="获取用户角色", notes="根据id查询用户角色",response = Map.class )
    @ApiImplicitParam(name = "id", value = "用户角色id",paramType = "path",required = true, dataType = "String")
    @RequestMapping(value={"/getById/{id}"}, method=RequestMethod.GET)
    public ResponseEntity getById(@PathVariable String id){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            UserRole UserRole = userRoleMapper.selectByPrimaryKey(Integer.parseInt(id));
            map.put("code", "0");
            map.put("data", UserRole);
            map.put("msg", "查询成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("data", "");
            map.put("msg", "查询失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="添加用户角色", notes="添加用户角色信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"id\":\"id\",\"userId\":\"11\",\"roleId\":\"1\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/add"}, method=RequestMethod.POST)
    public ResponseEntity add(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            UserRole UserRole = JSON.parseObject(jsonObject.toJSONString(), UserRole.class);
            Integer sum = userRoleMapper.insertSelective(UserRole);
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "添加成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum",0);
            map.put("msg", "添加失败！"+e.getMessage());
        }
        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="修改用户角色", notes="根据id修改用户角色信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"id\":\"id\",\"userId\":\"name\",\"roleId\":\"1\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/update"}, method=RequestMethod.POST)
    public ResponseEntity update(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            UserRole UserRole = JSON.parseObject(jsonObject.toJSONString(), UserRole.class);
            Integer sum = userRoleMapper.updateByPrimaryKeySelective(UserRole);
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "修改成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum",0);
            map.put("msg", "修改失败！"+e.getMessage());
        }
        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="删除用户角色", notes="批量删除用户角色",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"ids\":\"id1,id2\"}多个id以逗号隔开", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/deleteByIds"}, method=RequestMethod.POST)
    public ResponseEntity deleteByIds(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            String idstring = jsonObject.getString("ids");
            String[] ids = idstring.split(",");
            Integer sum = 0;
            for (String id:ids){
                userRoleMapper.deleteByPrimaryKey(Integer.parseInt(id));
                sum++;
            }
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "批量删除成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum", 0);
            map.put("msg", "批量删除失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }


}
