package com.ljw.service;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ljw.mapper.ApplicationMapper;
import com.ljw.model.Application;
import com.ljw.model.ApplicationExample;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 钜文 on 2017/1/21.
 */
@RestController
@Transactional
@RequestMapping(value="/application")
public class ApplicationService {

    @Autowired
    private ApplicationMapper applicationMapper;

    @ApiOperation(value="获取应用列表", notes="分页查询应用",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"start\":0,\"number\":\"10\",\"appName\":\"12\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/getAll"}, method=RequestMethod.POST)
    public ResponseEntity getAll(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            Integer start = jsonObject.getInteger("start");
            Integer number = jsonObject.getInteger("number");
            String appName = jsonObject.getString("appName");
            String ownerIds = jsonObject.getString("ownerIds");

            ApplicationExample applicationExample = new ApplicationExample();
            ApplicationExample.Criteria criteria= applicationExample.createCriteria();
            //分页
            applicationExample.setOffset(start);
            applicationExample.setLimit(number);
            //查询条件

            if (appName != null && !"".equals(appName)) {
                criteria.andAppNameLike("%"+appName+"%");
            }
            if (ownerIds != null && !"".equals(ownerIds)) {
                criteria.andOwnerIdsLike("%"+ownerIds+"%");
            }
            List<Application> list  = applicationMapper.selectByExample(applicationExample);
            Integer total = applicationMapper.countByExample(applicationExample);

            map.put("code", "0");
            map.put("total", total);
            map.put("data", list);
            map.put("msg", "查询成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("total",0);
            map.put("data", "");
            map.put("msg", "查询失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }
    

    @ApiOperation(value="获取应用", notes="根据id查询应用",response = Map.class )
    @ApiImplicitParam(name = "id", value = "应用id",paramType = "path",required = true, dataType = "String")
    @RequestMapping(value={"/getById/{id}"}, method=RequestMethod.GET)
    public ResponseEntity getById(@PathVariable String id){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            Application application = applicationMapper.selectByPrimaryKey(id);
            map.put("code", "0");
            map.put("data", application);
            map.put("msg", "查询成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("data", "");
            map.put("msg", "查询失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="添加应用", notes="添加应用信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"id\":\"id\",\"ownerIds\":\"1,2\",\"address\":\"name\",\"appName\":\"1\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/add"}, method=RequestMethod.POST)
    public ResponseEntity add(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{

            Application application = JSON.parseObject(jsonObject.toJSONString(), Application.class);
            Integer sum = applicationMapper.insertSelective(application);
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "添加成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum",0);
            map.put("msg", "添加失败！"+e.getMessage());
        }
        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="修改应用", notes="根据id修改应用信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"id\":\"id\",\"ownerIds\":\"1,2\",\"address\":\"name\",\"appName\":\"1\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/update"}, method=RequestMethod.POST)
    public ResponseEntity update(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            Application application = JSON.parseObject(jsonObject.toJSONString(), Application.class);
            Integer sum = applicationMapper.updateByPrimaryKeySelective(application);
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "修改成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum",0);
            map.put("msg", "修改失败！"+e.getMessage());
        }
        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="删除应用", notes="批量删除应用",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"ids\":\"id1,id2\"}多个id以逗号隔开", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/deleteByIds"}, method=RequestMethod.POST)
    public ResponseEntity deleteByIds(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            String idstring = jsonObject.getString("ids");
            String[] ids = idstring.split(",");
            Integer sum = 0;
            for (String id:ids){
                applicationMapper.deleteByPrimaryKey(id);
                sum++;
            }
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "批量删除成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum", 0);
            map.put("msg", "批量删除失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }


}
