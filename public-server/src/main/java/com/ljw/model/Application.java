package com.ljw.model;

import java.io.Serializable;

public class Application implements Serializable{
    /**
    * id
    */
    private String id;

    /**
    * 域名
    */
    private String address;

    /**
    * 应用名
    */
    private String appName;

    /**
    * 拥有者的账号
    */
    private String ownerIds;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getOwnerIds() {
        return ownerIds;
    }

    public void setOwnerIds(String ownerIds) {
        this.ownerIds = ownerIds;
    }
}