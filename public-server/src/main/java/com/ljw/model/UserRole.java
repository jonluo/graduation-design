package com.ljw.model;

import java.io.Serializable;

public class UserRole implements Serializable{
    /**
    * 自增id
    */
    private Integer id;

    /**
    * 角色名
    */
    private String roleName;

    /**
    * 用户账号
    */
    private String userAccount;

    /**
    * 角色id
    */
    private String roleId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }
}