package com.ljw.model;

import java.io.Serializable;

public class Permission implements Serializable{


    private Integer id;

    /**
    * 权限名
    */
    private String permissionname;

    /**
    * 角色id
    */
    private String roleId;

    /**
    * 权限键值
    */
    private String perKey;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPermissionname() {
        return permissionname;
    }

    public void setPermissionname(String permissionname) {
        this.permissionname = permissionname;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getPerKey() {
        return perKey;
    }

    public void setPerKey(String perKey) {
        this.perKey = perKey;
    }
}