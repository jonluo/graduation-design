package com.ljw;

import com.alibaba.fastjson.JSONObject;
import com.ljw.email.EmailService;
import com.ljw.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApiDemoApplicationTests {

	@Autowired
	private StringRedisTemplate stringRedisTemplate;
    @Autowired
	private EmailService emailService;

	@Test
	public void contextLoads() {

		// 保存字符串
		User u = new User();
		u.setId("111111");
		u.setUname("罗钜文");
		u.setAge(10);
		String json = JSONObject.toJSONString(u);
		stringRedisTemplate.opsForValue().set("userJson",json );
		String s  = stringRedisTemplate.opsForValue().get("userJson");
		System.out.println(s);
		Assert.assertEquals(json, stringRedisTemplate.opsForValue().get("userJson"));
	}

	@Test
	public void testEmail() throws Exception {
		//emailService.sendMail("1016167828@qq.com","测试主题","测试内容","");
	}

}
