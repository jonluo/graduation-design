<%--

    Licensed to Jasig under one or more contributor license
    agreements. See the NOTICE file distributed with this work
    for additional information regarding copyright ownership.
    Jasig licenses this file to you under the Apache License,
    Version 2.0 (the "License"); you may not use this file
    except in compliance with the License.  You may obtain a
    copy of the License at the following location:

      http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied.  See the License for the
    specific language governing permissions and limitations
    under the License.

--%>
<jsp:directive.include file="includes/top.jsp" />

<div id="login">
  <form:form method="post" id="fm1" commandName="${commandName}" htmlEscape="true" class="login-form">
    <form:errors path="*" id="msg" cssClass="alert text-center alert-danger in" element="div" htmlEscape="false" />
  <div class="login-wrap">
    <p class="login-img"><i class="icon_lock_alt"></i></p>
    <section class="input-group">
      <span class="input-group-addon"><i class="icon_profile"></i></span>

      <c:choose>
        <c:when test="${not empty sessionScope.openIdLocalId}">
          <strong>${sessionScope.openIdLocalId}</strong>
          <input type="hidden" id="username" name="username" value="${sessionScope.openIdLocalId}" />
        </c:when>
        <c:otherwise>
          <spring:message code="screen.welcome.label.netid.accesskey" var="userNameAccessKey" />
          <form:input cssClass="form-control" cssErrorClass="error" id="username" size="25" tabindex="1" accesskey="${userNameAccessKey}" path="username" autocomplete="off" htmlEscape="true" placeholder="Username"/>
        </c:otherwise>
      </c:choose>
    </section>
    
    <section class="input-group">
      <span class="input-group-addon"><i class="icon_key_alt"></i></span>
      <%--
      NOTE: Certain browsers will offer the option of caching passwords for a user.  There is a non-standard attribute,
      "autocomplete" that when set to "off" will tell certain browsers not to prompt to cache credentials.  For more
      information, see the following web page:
      http://www.technofundo.com/tech/web/ie_autocomplete.html
      --%>
      <spring:message code="screen.welcome.label.password.accesskey" var="passwordAccessKey" />
      <form:password cssClass="form-control" cssErrorClass="error" id="password" size="25" tabindex="2" path="password"  accesskey="${passwordAccessKey}" htmlEscape="true" autocomplete="off" placeholder="Password"/>
    </section>
    <div class="input-group" >
      <input class="form-control" style="width:50%" id="captcha" type="text" name="captcha" placeholder="Verification Code"/>
      <img style="width:50%;padding:0px 2px;" id="code" src="/cas/kaptcha" onclick="this.src='/cas/kaptcha?time='+Date.now()"  title="click refresh code">
    </div>

    <section>
      <input type="hidden" name="lt" value="${loginTicket}" />
      <input type="hidden" name="execution" value="${flowExecutionKey}" />
      <input type="hidden" name="_eventId" value="submit" />
      <input id="logbtn" class="btn btn-primary btn-lg btn-block" name="submit" accesskey="l" value="<spring:message code="screen.welcome.button.login" />" tabindex="4" type="submit" />
    </section>
  </div>
  </form:form>
</div>
<jsp:directive.include file="includes/bottom.jsp" />
