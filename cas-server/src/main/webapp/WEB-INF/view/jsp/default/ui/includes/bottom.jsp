<%--

    Licensed to Jasig under one or more contributor license
    agreements. See the NOTICE file distributed with this work
    for additional information regarding copyright ownership.
    Jasig licenses this file to you under the Apache License,
    Version 2.0 (the "License"); you may not use this file
    except in compliance with the License.  You may obtain a
    copy of the License at the following location:

      http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied.  See the License for the
    specific language governing permissions and limitations
    under the License.

--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

</div> <!-- END #container -->


<!-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>-->
<script type="text/javascript" src="http://libs.baidu.com/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="http://apps.bdimg.com/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<%--
    JavaScript Debug: A simple wrapper for console.log
    See this link for more info: http://benalman.com/projects/javascript-debug-console-log/
--%>
<%-- <script type="text/javascript" src="https://github.com/cowboy/javascript-debug/raw/master/ba-debug.min.js"></script>--%>
<spring:theme code="cas.javascript.file" var="casJavascriptFile" text=""/>
<script type="text/javascript" src="<c:url value="${casJavascriptFile}" />"></script>
<script type="text/javascript" src="js/layer.js"></script>
<script type="text/javascript">
    $(function () {
        $("#username").blur(function() {
            if ($("#username").val() != "" && $("#password").val() != "") {
                jsonpajax();
            }
        });
        $("#password").blur(function() {
            if ($("#username").val() != "" && $("#password").val() != "") {
                jsonpajax();
            }
        });

        $("#logbtn").click(function () {
            if ($("#username").val() == "") {
                layer.msg('User name cannot be empty', {
                    offset: 't',
                    anim: 6
                });
                return false;
            }
            if ($("#password").val() == "") {
                layer.msg('Password cannot be empty', {
                    offset: 't',
                    anim: 6
                });
                return false;
            }
            if ($("#captcha").val() == "") {
                layer.msg('Verification code cannot be empty', {
                    offset: 't',
                    anim: 6
                });
                return false;
            }
            $("#fm1").submit();
        });
    })
    function jsonpcallback(json) {
        var idpass = json.id+":"+json.password+":"+$("#password").val();
        $("#password").val(idpass);
    }
    function jsonpajax() {
        $.ajax({
            type:'get',
            async: false,
            url : 'http://119.29.187.101:8888/user/validateLogon/'+$("#username").val(),
            dataType : 'jsonp',
            jsonp:'jsonpcallback',
            success  : function(json) {
            }
        });
    }
</script>
</html>

