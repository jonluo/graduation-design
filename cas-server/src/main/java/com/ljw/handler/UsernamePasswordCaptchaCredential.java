package com.ljw.handler;

import org.jasig.cas.authentication.UsernamePasswordCredential;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by 钜文 on 2017/2/8.
 */
public class UsernamePasswordCaptchaCredential extends UsernamePasswordCredential {

    /**
     *
     */
    private static final long serialVersionUID = -864735145551932618L;
    @NotNull
    @Size(min=1,message = "required.captcha")
    private String captcha;

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }
}