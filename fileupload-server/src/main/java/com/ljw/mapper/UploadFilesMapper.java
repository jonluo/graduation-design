package com.ljw.mapper;

import com.ljw.model.UploadFiles;
import com.ljw.model.UploadFilesExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;
@Mapper
public interface UploadFilesMapper {
    @Cacheable(value="uploadFilesCount",key="'uploadFilesCount'+#p0.toString()")
    int countByExample(UploadFilesExample example);
    @Caching(evict = { @CacheEvict(value={"uploadFilesList","uploadFilesCount","uploadFiles"},allEntries=true)})
    int deleteByExample(UploadFilesExample example);
    @Caching(evict = { @CacheEvict(value={"uploadFilesList","uploadFilesCount"},allEntries=true),
            @CacheEvict(value="uploadFiles",key="#p0")})
    int deleteByPrimaryKey(String id);

    int insert(UploadFiles record);
    @Caching(evict = { @CacheEvict(value={"uploadFilesList","uploadFilesCount"},allEntries=true),
            @CacheEvict(value="uploadFiles",key="#p0.id")})
    int insertSelective(UploadFiles record);
    @Cacheable(value="uploadFilesList",key="'uploadFilesList'+#p0.toString()")
    List<UploadFiles> selectByExample(UploadFilesExample example);
    @Cacheable(value="uploadFiles",key="#p0")
    UploadFiles selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") UploadFiles record, @Param("example") UploadFilesExample example);

    int updateByExample(@Param("record") UploadFiles record, @Param("example") UploadFilesExample example);
    @Caching(evict = { @CacheEvict(value="uploadFilesList",allEntries=true),
            @CacheEvict(value="uploadFiles",key="#p0.id")})
    int updateByPrimaryKeySelective(UploadFiles record);

    int updateByPrimaryKey(UploadFiles record);
}