package com.ljw.service;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ljw.mapper.UploadFilesMapper;
import com.ljw.model.UploadFiles;
import com.ljw.model.UploadFilesExample;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 钜文 on 2017/1/21.
 */
@RestController
@Transactional
@RequestMapping(value="/uploadFiles")
public class UploadFilesService {

    @Autowired
    private UploadFilesMapper uploadFilesMapper;

    @ApiOperation(value="获取上传文件列表", notes="分页查询上传文件",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"pathNames\":\"x.jgp;\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/getAll"}, method=RequestMethod.POST)
    public ResponseEntity getAll(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{

            String[] pathNames = jsonObject.getString("pathNames").split(";");
            List<String> lsit = new ArrayList<String>();
            for (String name : pathNames){
                if (name!=null&&!"".equals(name)){
                    lsit.add(name);
                }
            }

            UploadFilesExample uploadFilesExample = new UploadFilesExample();
            UploadFilesExample.Criteria criteria= uploadFilesExample.createCriteria();
            //查询条件
            if (lsit != null && lsit.size()>0) {
                criteria.andPathNameIn(lsit);
            }

            List<UploadFiles> list  = uploadFilesMapper.selectByExample(uploadFilesExample);
            Integer total = uploadFilesMapper.countByExample(uploadFilesExample);

            map.put("code", "0");
            map.put("total", total);
            map.put("data", list);
            map.put("msg", "查询成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("total",0);
            map.put("data", "");
            map.put("msg", "查询失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }
    

    @ApiOperation(value="获取上传文件", notes="根据id查询上传文件",response = Map.class )
    @ApiImplicitParam(name = "id", value = "上传文件id",paramType = "path",required = true, dataType = "String")
    @RequestMapping(value={"/getById/{id}"}, method=RequestMethod.GET)
    public ResponseEntity getById(@PathVariable String id){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            UploadFiles uploadFiles = uploadFilesMapper.selectByPrimaryKey(id);
            map.put("code", "0");
            map.put("data", uploadFiles);
            map.put("msg", "查询成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("data", "");
            map.put("msg", "查询失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="添加上传文件", notes="添加上传文件信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"id\":\"id\",\"size\":100,\"size2\":\"size2\",\"uploadDate\":\"yyyy-mm-dd\",\"name\":\"12\",\"pathName\":\".jsp\",\"type\":\"1\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/add"}, method=RequestMethod.POST)
    public ResponseEntity add(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{

            UploadFiles uploadFiles = JSON.parseObject(jsonObject.toJSONString(), UploadFiles.class);
            Integer sum = uploadFilesMapper.insertSelective(uploadFiles);
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "添加成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum",0);
            map.put("msg", "添加失败！"+e.getMessage());
        }
        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="修改上传文件", notes="根据id修改上传文件信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"id\":\"id\",\"size\":100,\"size2\":\"size2\",\"uploadDate\":\"yyyy-mm-dd\",\"name\":\"12\",\"pathName\":\".jsp\",\"type\":\"1\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/update"}, method=RequestMethod.POST)
    public ResponseEntity update(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            UploadFiles uploadFiles = JSON.parseObject(jsonObject.toJSONString(), UploadFiles.class);
            Integer sum = uploadFilesMapper.updateByPrimaryKeySelective(uploadFiles);
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "修改成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum",0);
            map.put("msg", "修改失败！"+e.getMessage());
        }
        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="删除上传文件记录", notes="根据id批量删除上传文件",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"ids\":\"id1,id2\"}多个id以逗号隔开", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/deleteByIds"}, method=RequestMethod.POST)
    public ResponseEntity deleteByIds(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            String idstring = jsonObject.getString("ids");
            String[] ids = idstring.split(",");
            Integer sum = 0;
            for (String id:ids){
                uploadFilesMapper.deleteByPrimaryKey(id);
                sum++;
            }
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "批量删除成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum", 0);
            map.put("msg", "批量删除失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="删除上传文件记录", notes="根据物理名批量删除上传文件",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"pathName\":\"id1\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/deleteByPathName"}, method=RequestMethod.POST)
    public ResponseEntity deleteByPathName(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            String pathName = jsonObject.getString("pathName");

            UploadFilesExample uploadFilesExample = new UploadFilesExample();
            UploadFilesExample.Criteria criteria= uploadFilesExample.createCriteria();
            if (pathName != null && !"".equals(pathName)) {
                criteria.andPathNameEqualTo(pathName);
            }
            uploadFilesMapper.deleteByExample(uploadFilesExample);
            map.put("code", "0");
            map.put("msg", "批量删除成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("msg", "批量删除失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }

}
