# graduation-design

## PROJECT STRUCTURE Following
- app-gateway
  * 应用入口做负载均衡，端口：8180
- cas
  * cas单点，端口：8443
- config-server
  * 配置服务，端口：8000
- public-app
  * 公共服务app，端口：8080
- config-repertory
  * 配置仓库
- eureka-server
  * 服务注册发现，端口：8761
- public-server
  * 公共服务api，端口：8888
- fileupload-server
  * 文件上传服务，端口：9000
- sleuth-server
  * 分布式追踪，端口：9411
- film-server
  * 电影点评api，端口：8989
- film-app
  * 电影点评app，端口：8090
- homepage-app
  * 门户首页app，端口：8070
- docker
  * 容器编排
- DB-BAK
  * sql备份





## PS
> 这是一套基于springcloud的微服务架构系统
