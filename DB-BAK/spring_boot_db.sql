/*
Navicat MySQL Data Transfer

Source Server         : 腾讯云mysql
Source Server Version : 50635
Source Host           : 119.29.187.101:3306
Source Database       : spring_boot_db

Target Server Type    : MYSQL
Target Server Version : 50635
File Encoding         : 65001

Date: 2017-05-15 13:15:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `application`
-- ----------------------------
DROP TABLE IF EXISTS `application`;
CREATE TABLE `application` (
  `id` varchar(40) NOT NULL COMMENT 'id',
  `address` varchar(40) DEFAULT NULL COMMENT '域名',
  `app_name` varchar(40) DEFAULT NULL COMMENT '应用名',
  `owner_ids` varchar(1000) DEFAULT NULL COMMENT '拥有者账号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of application
-- ----------------------------
INSERT INTO `application` VALUES ('app00000104b20d3caf4b68960269f3c21311b1', 'http://localhost:8080', '智能分析', 'user001');
INSERT INTO `application` VALUES ('app00003744a3bc1672481c8a9669169488803e', 'http://localhost:8180/homepage-app', '主页', 'root');
INSERT INTO `application` VALUES ('app0000487e46b55fbf4731987244e662044530', 'http://localhost:8180/public-app', '公共服务', 'root');
INSERT INTO `application` VALUES ('app00009f63487ac02741f5a266852ddb72bcf6', 'http://localhost:8180/film-app', '电影点评', 'root');

-- ----------------------------
-- Table structure for `film`
-- ----------------------------
DROP TABLE IF EXISTS `film`;
CREATE TABLE `film` (
  `id` varchar(40) NOT NULL DEFAULT '' COMMENT 'id',
  `name` varchar(60) DEFAULT NULL COMMENT '电影名',
  `path_name` varchar(60) DEFAULT NULL COMMENT '图片路径',
  `director` varchar(20) DEFAULT NULL COMMENT '导演',
  `screenwriter` varchar(20) DEFAULT NULL COMMENT '编剧',
  `upload_date` date DEFAULT NULL COMMENT '上演时间',
  `starring` varchar(200) DEFAULT NULL COMMENT '主演',
  `type` varchar(20) DEFAULT NULL COMMENT '类型',
  `synopsis` varchar(1000) DEFAULT NULL COMMENT '剧情简介',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of film
-- ----------------------------
INSERT INTO `film` VALUES ('film000077ca478c8d0f4eca8e8a264afddf6422', '速度与激情8', 'file-032d79b12c504067a4219ae1d406dd75.jpeg;', 'F·加里·格雷', '克里斯·摩根', '2017-04-14', '范·迪塞尔，道恩·强森，杰森·斯坦森，查理兹·塞隆，米歇尔·罗德里格兹，娜塔莉·艾玛努埃尔', '动作、剧情、惊悚、犯罪', '多米尼克·托莱多（范·迪塞尔饰）与莱蒂（米歇尔·罗德里格兹饰）共度蜜月，布莱恩与米娅退出了赛车界，这支曾环游世界的顶级飞车家族队伍的生活正渐趋平淡。然而，一位神秘女子塞弗（查理兹·塞隆饰）的出现，她引诱多米尼克·托莱多走上犯罪道路，令整个队伍卷入信任与背叛的危机，生死患难的情义面临瓦解崩溃，前所未有的灾难考验着最个飞车家族[2]  。');
INSERT INTO `film` VALUES ('film000077ca478c8d0f4eca8e8a264afddf6423', '速度与激情8', 'file-032d79b12c504067a4219ae1d406dd75.jpeg;', 'F·加里·格雷', '克里斯·摩根', '2017-04-14', '范·迪塞尔，道恩·强森，杰森·斯坦森，查理兹·塞隆，米歇尔·罗德里格兹，娜塔莉·艾玛努埃尔', '动作、剧情、惊悚、犯罪', '多米尼克·托莱多（范·迪塞尔饰）与莱蒂（米歇尔·罗德里格兹饰）共度蜜月，布莱恩与米娅退出了赛车界，这支曾环游世界的顶级飞车家族队伍的生活正渐趋平淡。然而，一位神秘女子塞弗（查理兹·塞隆饰）的出现，她引诱多米尼克·托莱多走上犯罪道路，令整个队伍卷入信任与背叛的危机，生死患难的情义面临瓦解崩溃，前所未有的灾难考验着最个飞车家族[2]  。');
INSERT INTO `film` VALUES ('film000077ca478c8d0f4eca8e8a264afddf6424', '速度与激情8', 'file-032d79b12c504067a4219ae1d406dd75.jpeg;', 'F·加里·格雷', '克里斯·摩根', '2017-04-14', '范·迪塞尔，道恩·强森，杰森·斯坦森，查理兹·塞隆，米歇尔·罗德里格兹，娜塔莉·艾玛努埃尔', '动作、剧情、惊悚、犯罪', '多米尼克·托莱多（范·迪塞尔饰）与莱蒂（米歇尔·罗德里格兹饰）共度蜜月，布莱恩与米娅退出了赛车界，这支曾环游世界的顶级飞车家族队伍的生活正渐趋平淡。然而，一位神秘女子塞弗（查理兹·塞隆饰）的出现，她引诱多米尼克·托莱多走上犯罪道路，令整个队伍卷入信任与背叛的危机，生死患难的情义面临瓦解崩溃，前所未有的灾难考验着最个飞车家族[2]  。');
INSERT INTO `film` VALUES ('film000077ca478c8d0f4eca8e8a264afddf6425', '速度与激情8', 'file-032d79b12c504067a4219ae1d406dd75.jpeg;', 'F·加里·格雷', '克里斯·摩根', '2017-04-14', '范·迪塞尔，道恩·强森，杰森·斯坦森，查理兹·塞隆，米歇尔·罗德里格兹，娜塔莉·艾玛努埃尔', '动作、剧情、惊悚、犯罪', '多米尼克·托莱多（范·迪塞尔饰）与莱蒂（米歇尔·罗德里格兹饰）共度蜜月，布莱恩与米娅退出了赛车界，这支曾环游世界的顶级飞车家族队伍的生活正渐趋平淡。然而，一位神秘女子塞弗（查理兹·塞隆饰）的出现，她引诱多米尼克·托莱多走上犯罪道路，令整个队伍卷入信任与背叛的危机，生死患难的情义面临瓦解崩溃，前所未有的灾难考验着最个飞车家族[2]  。');
INSERT INTO `film` VALUES ('film000077ca478c8d0f4eca8e8a264afddf6426', '速度与激情8', 'file-032d79b12c504067a4219ae1d406dd75.jpeg;', 'F·加里·格雷', '克里斯·摩根', '2017-04-14', '范·迪塞尔，道恩·强森，杰森·斯坦森，查理兹·塞隆，米歇尔·罗德里格兹，娜塔莉·艾玛努埃尔', '动作、剧情、惊悚、犯罪', '多米尼克·托莱多（范·迪塞尔饰）与莱蒂（米歇尔·罗德里格兹饰）共度蜜月，布莱恩与米娅退出了赛车界，这支曾环游世界的顶级飞车家族队伍的生活正渐趋平淡。然而，一位神秘女子塞弗（查理兹·塞隆饰）的出现，她引诱多米尼克·托莱多走上犯罪道路，令整个队伍卷入信任与背叛的危机，生死患难的情义面临瓦解崩溃，前所未有的灾难考验着最个飞车家族[2]  。');
INSERT INTO `film` VALUES ('film000077ca478c8d0f4eca8e8a264afddf6428', '速度与激情8', 'file-032d79b12c504067a4219ae1d406dd75.jpeg;', 'F·加里·格雷', '克里斯·摩根', '2017-04-14', '范·迪塞尔，道恩·强森，杰森·斯坦森，查理兹·塞隆，米歇尔·罗德里格兹，娜塔莉·艾玛努埃尔', '动作、剧情、惊悚、犯罪', '多米尼克·托莱多（范·迪塞尔饰）与莱蒂（米歇尔·罗德里格兹饰）共度蜜月，布莱恩与米娅退出了赛车界，这支曾环游世界的顶级飞车家族队伍的生活正渐趋平淡。然而，一位神秘女子塞弗（查理兹·塞隆饰）的出现，她引诱多米尼克·托莱多走上犯罪道路，令整个队伍卷入信任与背叛的危机，生死患难的情义面临瓦解崩溃，前所未有的灾难考验着最个飞车家族[2]  。');
INSERT INTO `film` VALUES ('film000077ca478c8d0f4eca8e8a264afddf6429', '速度与激情8', 'file-032d79b12c504067a4219ae1d406dd75.jpeg;', 'F·加里·格雷', '克里斯·摩根', '2017-04-14', '范·迪塞尔，道恩·强森，杰森·斯坦森，查理兹·塞隆，米歇尔·罗德里格兹，娜塔莉·艾玛努埃尔', '动作、剧情、惊悚、犯罪', '多米尼克·托莱多（范·迪塞尔饰）与莱蒂（米歇尔·罗德里格兹饰）共度蜜月，布莱恩与米娅退出了赛车界，这支曾环游世界的顶级飞车家族队伍的生活正渐趋平淡。然而，一位神秘女子塞弗（查理兹·塞隆饰）的出现，她引诱多米尼克·托莱多走上犯罪道路，令整个队伍卷入信任与背叛的危机，生死患难的情义面临瓦解崩溃，前所未有的灾难考验着最个飞车家族[2]  。');
INSERT INTO `film` VALUES ('film000077ca478c8d0f4eca8e8a264afddf64e1', '速度与激情8', 'file-032d79b12c504067a4219ae1d406dd75.jpeg;', 'F·加里·格雷', '克里斯·摩根', '2017-04-14', '范·迪塞尔，道恩·强森，杰森·斯坦森，查理兹·塞隆，米歇尔·罗德里格兹，娜塔莉·艾玛努埃尔', '动作、剧情、惊悚、犯罪', '多米尼克·托莱多（范·迪塞尔饰）与莱蒂（米歇尔·罗德里格兹饰）共度蜜月，布莱恩与米娅退出了赛车界，这支曾环游世界的顶级飞车家族队伍的生活正渐趋平淡。然而，一位神秘女子塞弗（查理兹·塞隆饰）的出现，她引诱多米尼克·托莱多走上犯罪道路，令整个队伍卷入信任与背叛的危机，生死患难的情义面临瓦解崩溃，前所未有的灾难考验着最个飞车家族[2]  。');
INSERT INTO `film` VALUES ('film000077ca478c8d0f4eca8e8a264afddf64e2', '速度与激情8', 'file-032d79b12c504067a4219ae1d406dd75.jpeg;', 'F·加里·格雷', '克里斯·摩根', '2017-04-14', '范·迪塞尔，道恩·强森，杰森·斯坦森，查理兹·塞隆，米歇尔·罗德里格兹，娜塔莉·艾玛努埃尔', '动作、剧情、惊悚、犯罪', '多米尼克·托莱多（范·迪塞尔饰）与莱蒂（米歇尔·罗德里格兹饰）共度蜜月，布莱恩与米娅退出了赛车界，这支曾环游世界的顶级飞车家族队伍的生活正渐趋平淡。然而，一位神秘女子塞弗（查理兹·塞隆饰）的出现，她引诱多米尼克·托莱多走上犯罪道路，令整个队伍卷入信任与背叛的危机，生死患难的情义面临瓦解崩溃，前所未有的灾难考验着最个飞车家族[2]  。');
INSERT INTO `film` VALUES ('film000077ca478c8d0f4eca8e8a264afddf64e3', '速度与激情8', 'file-032d79b12c504067a4219ae1d406dd75.jpeg;', 'F·加里·格雷', '克里斯·摩根', '2017-04-14', '范·迪塞尔，道恩·强森，杰森·斯坦森，查理兹·塞隆，米歇尔·罗德里格兹，娜塔莉·艾玛努埃尔', '动作、剧情、惊悚、犯罪', '多米尼克·托莱多（范·迪塞尔饰）与莱蒂（米歇尔·罗德里格兹饰）共度蜜月，布莱恩与米娅退出了赛车界，这支曾环游世界的顶级飞车家族队伍的生活正渐趋平淡。然而，一位神秘女子塞弗（查理兹·塞隆饰）的出现，她引诱多米尼克·托莱多走上犯罪道路，令整个队伍卷入信任与背叛的危机，生死患难的情义面临瓦解崩溃，前所未有的灾难考验着最个飞车家族[2]  。');
INSERT INTO `film` VALUES ('film000077ca478c8d0f4eca8e8a264afddf64e4', '速度与激情8', 'file-032d79b12c504067a4219ae1d406dd75.jpeg;', 'F·加里·格雷', '克里斯·摩根', '2017-04-14', '范·迪塞尔，道恩·强森，杰森·斯坦森，查理兹·塞隆，米歇尔·罗德里格兹，娜塔莉·艾玛努埃尔', '动作、剧情、惊悚、犯罪', '多米尼克·托莱多（范·迪塞尔饰）与莱蒂（米歇尔·罗德里格兹饰）共度蜜月，布莱恩与米娅退出了赛车界，这支曾环游世界的顶级飞车家族队伍的生活正渐趋平淡。然而，一位神秘女子塞弗（查理兹·塞隆饰）的出现，她引诱多米尼克·托莱多走上犯罪道路，令整个队伍卷入信任与背叛的危机，生死患难的情义面临瓦解崩溃，前所未有的灾难考验着最个飞车家族[2]  。');
INSERT INTO `film` VALUES ('film000077ca478c8d0f4eca8e8a264afddf64e5', '速度与激情8', 'file-032d79b12c504067a4219ae1d406dd75.jpeg;', 'F·加里·格雷', '克里斯·摩根', '2017-04-14', '范·迪塞尔，道恩·强森，杰森·斯坦森，查理兹·塞隆，米歇尔·罗德里格兹，娜塔莉·艾玛努埃尔', '动作、剧情、惊悚、犯罪', '多米尼克·托莱多（范·迪塞尔饰）与莱蒂（米歇尔·罗德里格兹饰）共度蜜月，布莱恩与米娅退出了赛车界，这支曾环游世界的顶级飞车家族队伍的生活正渐趋平淡。然而，一位神秘女子塞弗（查理兹·塞隆饰）的出现，她引诱多米尼克·托莱多走上犯罪道路，令整个队伍卷入信任与背叛的危机，生死患难的情义面临瓦解崩溃，前所未有的灾难考验着最个飞车家族[2]  。');
INSERT INTO `film` VALUES ('film000077ca478c8d0f4eca8e8a264afddf64e6', '速度与激情8', 'file-032d79b12c504067a4219ae1d406dd75.jpeg;', 'F·加里·格雷', '克里斯·摩根', '2017-04-14', '范·迪塞尔，道恩·强森，杰森·斯坦森，查理兹·塞隆，米歇尔·罗德里格兹，娜塔莉·艾玛努埃尔', '动作、剧情、惊悚、犯罪', '多米尼克·托莱多（范·迪塞尔饰）与莱蒂（米歇尔·罗德里格兹饰）共度蜜月，布莱恩与米娅退出了赛车界，这支曾环游世界的顶级飞车家族队伍的生活正渐趋平淡。然而，一位神秘女子塞弗（查理兹·塞隆饰）的出现，她引诱多米尼克·托莱多走上犯罪道路，令整个队伍卷入信任与背叛的危机，生死患难的情义面临瓦解崩溃，前所未有的灾难考验着最个飞车家族[2]  。');
INSERT INTO `film` VALUES ('film000077ca478c8d0f4eca8e8a264afddf64e7', '速度与激情8', 'file-032d79b12c504067a4219ae1d406dd75.jpeg;', 'F·加里·格雷', '克里斯·摩根', '2017-04-14', '范·迪塞尔，道恩·强森，杰森·斯坦森，查理兹·塞隆，米歇尔·罗德里格兹，娜塔莉·艾玛努埃尔', '动作、剧情、惊悚、犯罪', '多米尼克·托莱多（范·迪塞尔饰）与莱蒂（米歇尔·罗德里格兹饰）共度蜜月，布莱恩与米娅退出了赛车界，这支曾环游世界的顶级飞车家族队伍的生活正渐趋平淡。然而，一位神秘女子塞弗（查理兹·塞隆饰）的出现，她引诱多米尼克·托莱多走上犯罪道路，令整个队伍卷入信任与背叛的危机，生死患难的情义面临瓦解崩溃，前所未有的灾难考验着最个飞车家族[2]  。');
INSERT INTO `film` VALUES ('film000077ca478c8d0f4eca8e8a264afddf64e8', '速度与激情8', 'file-032d79b12c504067a4219ae1d406dd75.jpeg;', 'F·加里·格雷', '克里斯·摩根', '2017-04-14', '范·迪塞尔，道恩·强森，杰森·斯坦森，查理兹·塞隆，米歇尔·罗德里格兹，娜塔莉·艾玛努埃尔', '动作、剧情、惊悚、犯罪', '多米尼克·托莱多（范·迪塞尔饰）与莱蒂（米歇尔·罗德里格兹饰）共度蜜月，布莱恩与米娅退出了赛车界，这支曾环游世界的顶级飞车家族队伍的生活正渐趋平淡。然而，一位神秘女子塞弗（查理兹·塞隆饰）的出现，她引诱多米尼克·托莱多走上犯罪道路，令整个队伍卷入信任与背叛的危机，生死患难的情义面临瓦解崩溃，前所未有的灾难考验着最个飞车家族[2]  。');
INSERT INTO `film` VALUES ('film000077ca478c8d0f4eca8e8a264afddf64e9', '速度与激情8', 'file-032d79b12c504067a4219ae1d406dd75.jpeg;', 'F·加里·格雷', '克里斯·摩根', '2017-04-14', '范·迪塞尔，道恩·强森，杰森·斯坦森，查理兹·塞隆，米歇尔·罗德里格兹，娜塔莉·艾玛努埃尔', '动作、剧情、惊悚、犯罪', '多米尼克·托莱多（范·迪塞尔饰）与莱蒂（米歇尔·罗德里格兹饰）共度蜜月，布莱恩与米娅退出了赛车界，这支曾环游世界的顶级飞车家族队伍的生活正渐趋平淡。然而，一位神秘女子塞弗（查理兹·塞隆饰）的出现，她引诱多米尼克·托莱多走上犯罪道路，令整个队伍卷入信任与背叛的危机，生死患难的情义面临瓦解崩溃，前所未有的灾难考验着最个飞车家族[2]  。');

-- ----------------------------
-- Table structure for `film_review`
-- ----------------------------
DROP TABLE IF EXISTS `film_review`;
CREATE TABLE `film_review` (
  `id` varchar(60) NOT NULL DEFAULT '' COMMENT 'id',
  `film_name` varchar(60) DEFAULT NULL COMMENT '电影名',
  `film_id` varchar(60) DEFAULT NULL COMMENT '电影id',
  `user_account` varchar(20) DEFAULT NULL COMMENT '用户账号',
  `date` datetime DEFAULT NULL COMMENT '评论时间',
  `level` int(2) DEFAULT NULL COMMENT '类型',
  `content` varchar(1000) DEFAULT NULL COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of film_review
-- ----------------------------
INSERT INTO `film_review` VALUES ('filmReview0000190a55f7b15a4e9bba4feda6ca55316a', ' 速度与激情8', 'film000077ca478c8d0f4eca8e8a264afddf6422', '111111', '2017-05-14 21:09:20', '4', '跟5和7没法比，场面创意已近枯竭，“遥控车”和“汽车雨”哪新奇了？分别升级自《终结者3》和《2012》嘛！不过近半小时潜艇大战是值回票价的...最难接受是为凑明星狂洗白Shaw兄弟，上集丧心病狂斯坦森如今丧心病狂卖萌，与family牵手进餐，考虑过韩和神奇女侠的感受吗？这狗屁family也太胡闹了！5.5/10');
INSERT INTO `film_review` VALUES ('filmReview00002523f69dd61d4cf688b616d18c4350d6', ' 速度与激情8', 'film000077ca478c8d0f4eca8e8a264afddf6422', '111111', '2017-05-14 21:09:49', '3', '所以导演想用一个塞隆留住男性颜狗观众，就彻底不管女性颜狗的感受了呗？没了保罗沃克，三个大块头光头真的吃不下啊！');
INSERT INTO `film_review` VALUES ('filmReview00002757df2f16694b1384046d356063cb09', ' 速度与激情8', 'film000077ca478c8d0f4eca8e8a264afddf6422', '111111', '2017-05-14 21:03:16', '5', '嗖嗖嗖，砰砰砰，duang duang duang, we are 伐木累！电影结束！');
INSERT INTO `film_review` VALUES ('filmReview0000430e3e024152410f84d1fc5d74ff5d66', ' 速度与激情8', 'film000077ca478c8d0f4eca8e8a264afddf6422', '111111', '2017-05-14 21:12:49', '4', '速度与激情就是老太太的裹脚布，又臭又长，有完没完。');
INSERT INTO `film_review` VALUES ('filmReview00006c55b3c5859a40a292b34314fa51db35', ' 速度与激情8', 'film000077ca478c8d0f4eca8e8a264afddf6423', '111111', '2017-05-14 21:16:00', '4', '《速度与激情8》这部电影最近在4月14号在国内上映，现在才过了四天，国内累计票房已经到了14亿。在同期上映的电影里面，这已经算是压倒性的优势了。当然，这么高的票房，还是有原因的。');
INSERT INTO `film_review` VALUES ('filmReview00009e6eb7cf41184cf89a2149bcc0716cdd', ' 速度与激情8', 'film000077ca478c8d0f4eca8e8a264afddf6422', '111111', '2017-05-15 11:05:27', '3', '123123');
INSERT INTO `film_review` VALUES ('filmReview0000a4d6ec2f51fe4cc0b3706d9289f7174f', ' 速度与激情8', 'film000077ca478c8d0f4eca8e8a264afddf6424', '111111', '2017-05-14 21:58:52', '4', 'ggggggggggggggggggggggggggggood');
INSERT INTO `film_review` VALUES ('filmReview0000dbcc1793cba6439e9631a90343330f7a', ' 速度与激情8', 'film000077ca478c8d0f4eca8e8a264afddf6422', '111111', '2017-05-15 13:13:16', '2', '111111111111111');
INSERT INTO `film_review` VALUES ('filmReview0000f4caf4a7d1514f1195758864806470c2', ' 速度与激情8', 'film000077ca478c8d0f4eca8e8a264afddf6422', '111111', '2017-05-14 21:03:38', '4', '欢迎观看《速度与激情8（F8）》，又名《速度与光头》、《比F4厉害两倍的团队》、《无发可说》、《我的“光头光环”升到了满级》、《头发越短越容易活下来》，以及《有种动我一根头发试试》。长篇影评：https://movie.douban.com/review/8476912/');
INSERT INTO `film_review` VALUES ('filmReview0000f5cd8881263a4cbf8c7668128ff65799', ' 速度与激情8', 'film000077ca478c8d0f4eca8e8a264afddf6422', '111111', '2017-05-15 13:12:39', '1', '11111111111111111111');

-- ----------------------------
-- Table structure for `hot_search`
-- ----------------------------
DROP TABLE IF EXISTS `hot_search`;
CREATE TABLE `hot_search` (
  `id` varchar(60) NOT NULL DEFAULT '' COMMENT 'id',
  `search_content` varchar(200) DEFAULT NULL COMMENT '搜索内容',
  `times` int(10) DEFAULT NULL COMMENT '次数',
  `date` date DEFAULT NULL COMMENT '搜索时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hot_search
-- ----------------------------
INSERT INTO `hot_search` VALUES ('hotSearch00001cbd089901d44a20ae53a823f96b6041', '速度', '10', '2017-05-14');
INSERT INTO `hot_search` VALUES ('hotSearch000025f6e662407e4018a5afc8ac10d781d2', '进击的巨人', '16', '2017-05-14');
INSERT INTO `hot_search` VALUES ('hotSearch000055404528ca6e454294d3dc1510328d22', '越狱', '6', '2017-05-14');
INSERT INTO `hot_search` VALUES ('hotSearch0000715c311d3af24080ab56b3d854d9e706', '速度与激情8', '24', '2017-05-14');
INSERT INTO `hot_search` VALUES ('hotSearch0000756d84ac83ef4145847dc42c7ac0a35b', '行尸走肉', '12', '2017-05-14');
INSERT INTO `hot_search` VALUES ('hotSearch00007d2b55c2f78242a99a08dc6886ac5dae', '火影忍者', '10', '2017-05-14');
INSERT INTO `hot_search` VALUES ('hotSearch000083488268119c45ed90017f3201e40601', '人民的名义', '9', '2017-05-14');
INSERT INTO `hot_search` VALUES ('hotSearch0000afed6afc176e4db5b918494e09bc5f64', '秦时明月', '9', '2017-05-14');
INSERT INTO `hot_search` VALUES ('hotSearch0000f737a4814fe0457e94cc05d3e5c2ff9f', '海贼王', '7', '2017-05-14');

-- ----------------------------
-- Table structure for `permission`
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionname` varchar(32) DEFAULT NULL COMMENT '权限名',
  `role_id` varchar(60) DEFAULT NULL COMMENT '角色id',
  `per_key` varchar(20) NOT NULL COMMENT '权限键值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES ('12', '超级权限', 'userRole855964', 'admin');
INSERT INTO `permission` VALUES ('14', '普通权限', 'userRole756602', 'user');

-- ----------------------------
-- Table structure for `upload_files`
-- ----------------------------
DROP TABLE IF EXISTS `upload_files`;
CREATE TABLE `upload_files` (
  `id` varchar(40) NOT NULL DEFAULT '' COMMENT 'id',
  `name` varchar(100) DEFAULT NULL COMMENT '文件名',
  `path_name` varchar(60) DEFAULT NULL COMMENT '物理名',
  `type` varchar(60) DEFAULT NULL COMMENT '类型',
  `size2` varchar(20) DEFAULT NULL COMMENT '大小字符串',
  `upload_date` date DEFAULT NULL COMMENT '上传时间',
  `size` int(10) DEFAULT NULL COMMENT '大小',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of upload_files
-- ----------------------------
INSERT INTO `upload_files` VALUES ('file00000161455a39434491a8911c13b53bc07f', '57ba60af440bb.gif', 'file-50eeba7af34c4db7a41bd41c915f6d77.gif', 'image/gif', '368 KB', '2017-05-07', '367793');
INSERT INTO `upload_files` VALUES ('file0000062e0ec642094ed5989eecae2dfa5c96', '20160205001203_whScZ.thumb.700_0.jpeg', 'file-032d79b12c504067a4219ae1d406dd75.jpeg', 'image/jpeg', '135 KB', '2017-05-12', '134680');
INSERT INTO `upload_files` VALUES ('file00000eb86dceeedd422a9da5c74bc07b7663', '57ba60a688156.gif', 'file-20fe78fc2f6c4130ad5756c6d33e0b9f.gif', 'image/gif', '569 KB', '2017-05-07', '569499');
INSERT INTO `upload_files` VALUES ('file0000226b5b150f3a47bc90a976ff830ec8a6', '57ba60a7ec63c.gif', 'file-030b823223514975afc37add9631e8b1.gif', 'image/gif', '425 KB', '2017-05-07', '425215');
INSERT INTO `upload_files` VALUES ('file00002c3171bc8f264aacb939016343e9517b', '57ba60a7ec63c.gif', 'file-41fc0deb50444df4b2710b5eae0bb469.gif', 'image/gif', '425 KB', '2017-05-07', '425215');
INSERT INTO `upload_files` VALUES ('file000047e69fcced5e440aa20c3b51518ca2a4', '57ba60a688156.gif', 'file-27a330bb91b7434d918c70b8372086f3.gif', 'image/gif', '569 KB', '2017-05-07', '569499');
INSERT INTO `upload_files` VALUES ('file000052152f5a7d93403c810a854dab330997', '57ba60af440bb.gif', 'file-7c57419042364b258ccda82b93431294.gif', 'image/gif', '368 KB', '2017-05-07', '367793');
INSERT INTO `upload_files` VALUES ('file0000b1825a6a199e4b848793c64e8fb89f24', '57ba60a79bc03.gif', 'file-f2604ab6d27e473abc5fb265280075ec.gif', 'image/gif', '303 KB', '2017-05-07', '303486');
INSERT INTO `upload_files` VALUES ('file0000b20495b17ef546a8a17579730fbb28e6', '57ba60af440bb.gif', 'file-52fcf08b724f4610a5e81824bf44b776.gif', 'image/gif', '368 KB', '2017-05-07', '367793');
INSERT INTO `upload_files` VALUES ('file0000b9f57631425a495892d37115c11de8fb', '57ba60af440bb.gif', 'file-06332998404b4f17819cb2d98d1fd5f5.gif', 'image/gif', '368 KB', '2017-05-07', '367793');
INSERT INTO `upload_files` VALUES ('file0000c2763b6eec8f4afdb8b6a86ad54551a8', '57ba60a7ec63c.gif', 'file-1ee27363066545d785790802e06e10bd.gif', 'image/gif', '425 KB', '2017-05-07', '425215');
INSERT INTO `upload_files` VALUES ('file0000c349c5105bca4a14949fa3b0a59b75cf', '57ba60af440bb.gif', 'file-da94a29bca084878b7dacaa48b830d34.gif', 'image/gif', '368 KB', '2017-05-07', '367793');
INSERT INTO `upload_files` VALUES ('file0000e6320e4d2aac4cdfa5ea44c9aca3fe99', '57ba60a8b9afc.gif', 'file-b4e11e2b6b3e4234a2184c9edc22de2f.gif', 'image/gif', '546 KB', '2017-05-07', '545764');
INSERT INTO `upload_files` VALUES ('file0000ead30969f70742acbc99a84131f5692a', '5.jpg', 'file-86acb48172a544589388a74ce44f4051.jpg', 'image/jpeg', '828 KB', '2017-05-07', '827901');
INSERT INTO `upload_files` VALUES ('file0000eee0e09afdad403f8896b6e91a503c10', '57ba60af440bb.gif', 'file-b282d02611d14a608a1dbfa9aec6084a.gif', 'image/gif', '368 KB', '2017-05-07', '367793');
INSERT INTO `upload_files` VALUES ('file0000fbf57cb9cfa2458594492ea53ccaa485', '57ba60a942a29.gif', 'file-7011951afff3461783c9196880487cf1.gif', 'image/gif', '520 KB', '2017-05-07', '520267');
INSERT INTO `upload_files` VALUES ('file0000fc290f9e5d874933a434004449e7660a', '57ba60a713b5f.gif', 'file-f16af0a4f0ea415faab52dcf497f8a09.gif', 'image/gif', '507 KB', '2017-05-07', '507234');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` varchar(40) NOT NULL COMMENT 'id',
  `uname` varchar(40) DEFAULT NULL COMMENT '名字',
  `age` int(10) DEFAULT NULL COMMENT '年龄',
  `tel` varchar(11) DEFAULT NULL COMMENT '手机号',
  `account` varchar(40) DEFAULT NULL COMMENT '账号',
  `email` varchar(40) DEFAULT NULL COMMENT '邮箱',
  `password` varchar(60) DEFAULT NULL COMMENT '密码',
  `sex` int(2) DEFAULT NULL COMMENT '性别 0男 1女',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('abc111', '罗钜文', '2', '1593515947', 'root', '10@qq.com', '0F1EEB8128AEB57BD46542573C57F60E57DB', '0');
INSERT INTO `user` VALUES ('user00000c2212ecfe2d49a38a7088b46dba5867', null, null, '11111111111', '222222', '11@qq.com', 'D50C3D51B89F46B4E33D398B4D73B9BDD9E7', '0');
INSERT INTO `user` VALUES ('user00001b368a679a604fe9912d7e8f7f66605d', null, null, '11111111111', '333333', '1@qq.com', '7EDFEDC0A66AC230DE11885E9C93DDDD8748', '0');
INSERT INTO `user` VALUES ('user00002aac7cae48df4f78bcc33d02af664c62', null, null, '11111111111', '111111', '11@qq.com', 'B03AC21C000C5A629DC78DB61A6D1D99A6A3', '0');
INSERT INTO `user` VALUES ('user00002e74606369ca4d5a8d9ca587768215bf', '用户', '22', '11111111111', 'user001', '1016167828@qq.com', '8839EC967B22F82CD35304B59FDF2DB6D729', '0');

-- ----------------------------
-- Table structure for `user_role`
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `role_name` varchar(20) DEFAULT NULL COMMENT '角色名',
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `user_account` varchar(60) DEFAULT NULL COMMENT '用户账号',
  `role_id` varchar(60) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES ('普通用户', '28', 'user001', 'userRole756602');
INSERT INTO `user_role` VALUES ('管理员', '30', 'root', 'userRole855964');
