package com.ljw.utils;


import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Web相关工具类,同时用spring session，因此用户信息也放到session和cookie里
 */
@Service
public  class WebUtils {

	//cookie的键
	private final static String USER_COOKIE_KEY ="uid";
	//cookie的密钥
	private final static String USER_COOKIE_SECRET ="my-sec&#%!&*";
	/**
	 * 返回当前用户
	 * @param request
	 * @param response
	 * @return
	 */
	public User currentUserSession(HttpServletRequest request, HttpServletResponse response) {
		User user = (User)request.getSession().getAttribute("film");
		return user;
	}

	/**
	 * 返回当前用户id
	 * @param request
	 * @param response
	 * @return
	 */
	public String currentUserCookie(HttpServletRequest request, HttpServletResponse response) {

		// 获取cookie信息
		String userCookie = getCookie(request, USER_COOKIE_KEY);
		// 1.cookie为空，直接清除
		if (StringUtils.isEmpty(userCookie)) {
			removeCookie(response, USER_COOKIE_KEY);
			return null;
		}
		String cookieInfo = null;
		// 2.解密cookie
		try {
			cookieInfo = new DesUtils(USER_COOKIE_SECRET).decryptString(userCookie);
		} catch (RuntimeException e) {
			// ignore
		}
		// 3.异常或解密问题，直接清除cookie信息
		if (StringUtils.isEmpty(cookieInfo)) {
			removeCookie(response, USER_COOKIE_KEY);
			return null;
		}
		String[] userInfo = cookieInfo.split("-");
		// 4.规则不匹配
		if (userInfo.length < 3) {
			removeCookie(response, USER_COOKIE_KEY);
			return null;
		}
		String userId   = userInfo[0];
		String oldTime  = userInfo[1];
		String maxAge   = userInfo[2];
		// 5.判定时间区间，超时的cookie清理掉
		if (!"-1".equals(maxAge)) {
			long now  = System.currentTimeMillis();
			long time = Long.parseLong(oldTime) + (Long.parseLong(maxAge) * 1000);
			if (time <= now) {
				removeCookie(response, USER_COOKIE_KEY);
				return null;
			}
		}
		if(StringUtils.isEmpty(userId)){
			removeCookie(response, USER_COOKIE_KEY);
			return null;
		}
		return userId;
	}

	/**
	 * 用户登陆状态维持
	 * cookie设计为: des(私钥).encode(userId-time-maxAge-ip)
	 * @param request
	 * @param  user
	 * @param remember   记住密码默认为1天
	 * @return void
	 */
	public static void loginUser(HttpServletRequest request, HttpServletResponse response, User user, boolean remember) {
		
		request.getSession().setAttribute("user", user);
		// 获取用户的id
		String uid = user.getId()+"";
		// 当前毫秒数
		long now = System.currentTimeMillis();
		// 超时时间
		int  maxAge   = -1;
		if (remember) {
			maxAge = 60 * 60 * 24 * 1; // 1天
		}
		// 用户id地址
		String ip = getIP(request);
		// 构造cookie
		StringBuilder cookieBuilder = new StringBuilder()
			.append(uid).append("-")
			.append(now).append("-")
			.append(maxAge).append("-")
			.append(ip);
		// 加密cookie
		String userCookie = new DesUtils(USER_COOKIE_SECRET).encryptString(cookieBuilder.toString());
		// 设置用户的cookie、 maxAge=-1 维持成session的状态
		setCookie(response, USER_COOKIE_KEY, userCookie, maxAge);
	}

	/**
	 * 退出即删除用户信息
	 * @param
	 * @return void
	 */
	public static void logoutUser(HttpServletRequest request,HttpServletResponse response) {
		request.getSession().removeAttribute("film");
		removeCookie(response,USER_COOKIE_KEY);
		
	}

	/**
	 * 读取cookie
	 * @param request
	 * @param key
	 * @return
	 */
	public static String getCookie(HttpServletRequest request, String key) {
		Cookie[] cookies = request.getCookies();
		if(null != cookies){
			for (Cookie cookie : cookies) {
				if (key.equals(cookie.getName())) {
					return cookie.getValue();
				}
			}
		}
		return null;
	}

	/**
	 * 清除 某个指定的cookie 
	 * @param response
	 * @param key
	 */
	public static void removeCookie(HttpServletResponse response, String key) {
		setCookie(response, key, null, 0);
	}

	/**
	 * 设置cookie
	 * @param response
	 * @param name
	 * @param value
	 * @param maxAgeInSeconds
	 */
	public static void setCookie(HttpServletResponse response, String name, String value, int maxAgeInSeconds) {
		Cookie cookie = new Cookie(name, value);
		cookie.setPath("/");
		cookie.setMaxAge(maxAgeInSeconds);
		// 指定为httpOnly保证安全性
		cookie.setHttpOnly(true);
		response.addCookie(cookie);
	}



	/**
	 * 获取浏览器信息
	 * @param request
	 * @return String
	 */
	public static String getUserAgent(HttpServletRequest request) {
		return request.getHeader("User-Agent");
	}

	/**
	 * 获取ip
	 * @param request
	 * @return
	 */
	public static String getIP(HttpServletRequest request) {
		String ip = request.getHeader("X-Requested-For");
		if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("X-Forwarded-For");
		}
		if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

}
