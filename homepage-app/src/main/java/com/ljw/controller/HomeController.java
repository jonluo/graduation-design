package com.ljw.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ljw.utils.UuidUtils;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 钜文 on 2017/1/19.
 */
@Controller
@RequestMapping("/home")
public class HomeController {

    private Logger logger = Logger.getLogger(HomeController.class);

    @Autowired
    RestTemplate restTemplate;

    @Value("${film.service.url}")
    private String filmServiceUrl;


    @RequestMapping("/film")
    public String film(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
            String resultJson =restTemplate.getForEntity(filmServiceUrl+"/film/getById/"+request.getParameter("fid"), String.class).getBody();
            //String resultJson = HttpUtil.post(baseApiUrl + "/film/deleteByIds",json.toJSONString());
            resultMap = JSON.parseObject(resultJson);
            request.setAttribute("film",resultMap.get("data"));
        } catch (Exception e) {
            resultMap.put("msg", e.getMessage());
            resultMap.put("code", "1");
            logger.error(e.getMessage());
        }

        return "film";
    }


    /**
     * 根据id查找
     * @param request
     * @return
     */
    @RequestMapping("/getScoreByFilmId")
    @ResponseBody
    @HystrixCommand(fallbackMethod = "fallback")
    public Map<String, Object> getScoreByFilmId(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        Map<String, Object> scoreMap = new HashMap<String, Object>();
        try {
            String resultJson =restTemplate.getForEntity(filmServiceUrl+"/filmReview/getByFilmId/"+request.getParameter("fid"), String.class).getBody();
            //String resultJson = HttpUtil.post(baseApiUrl + "/filmReview/deleteByIds",json.toJSONString());
            resultMap = JSON.parseObject(resultJson);
            List<Map<String, Object>> scoreList= (List<Map<String, Object>>) resultMap.get("data");

            if (scoreList.size()>0){
                //加权平均
                int sum = 0;
                double score = 0;
                for (Map map:scoreList){
                    scoreMap.put("s"+map.get("level"),map.get("total"));
                    score += Integer.parseInt(map.get("level")+"") * Integer.parseInt(map.get("total")+"");
                    sum += Integer.parseInt(map.get("total")+"");
                }
                NumberFormat nf=new DecimalFormat( "0.0 ");
                score =Double.parseDouble(nf.format((score/sum)*2));
                scoreMap.put("score",score);
                scoreMap.put("total",sum);
                resultMap.put("data",scoreMap);
            }

        } catch (Exception e) {
            resultMap.put("msg", e.getMessage());
            resultMap.put("code", "1");
            logger.error(e.getMessage());
        }
        return resultMap;
    }
    /**
     * 分页查询
     *
     * @param request
     * @return
     */
    @RequestMapping("/reviewlist")
    @ResponseBody
    @HystrixCommand(fallbackMethod = "fallback")
    public Map<String, Object> reviewlist(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
            // 执行查询
            JSONObject json = new JSONObject();
            Integer pageNo = Integer.parseInt(request.getParameter("pageNo"));
            Integer pageSize = Integer.parseInt(request.getParameter("pageSize"));
            json.put("start",(pageNo-1)*pageSize);
            json.put("number",pageSize);
            json.put("filmId",request.getParameter("filmId"));
            String resultJson = restTemplate.postForEntity(filmServiceUrl+ "/filmReview/getAll",json, String.class).getBody();
            // String resultJson = HttpUtil.post(baseApiUrl+"/filmReview/getAll",json.toJSONString());
            resultMap = JSON.parseObject(resultJson);
            // 总页数
            int count = (int) resultMap.get("total");
            resultMap.put("totalPage", (count % pageSize == 0) ? count / pageSize : count / pageSize + 1);
            // 当前页数
            resultMap.put("curPage",pageNo);
            // 每页记录数
            resultMap.put("pageLine", pageSize);
        } catch (Exception e) {
            resultMap.put("msg", e.getMessage());
            resultMap.put("code", "1");
            logger.error(e.getMessage());
        }
        return resultMap;
    }

    /**
     * 分页查询
     *
     * @param request
     * @return
     */
    @RequestMapping("/list")
    @ResponseBody
    @HystrixCommand(fallbackMethod = "fallback")
    public Map<String, Object> list(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
            // 执行查询
            JSONObject json = new JSONObject();
            Integer pageNo = Integer.parseInt(request.getParameter("pageNo"));
            Integer pageSize = Integer.parseInt(request.getParameter("pageSize"));
            String more = request.getParameter("more");
            json.put("start",(pageNo-1)*pageSize);
            json.put("number",pageSize);
            json.put("more",more);
            //添加记录
            if (more !=null && !"".equals(more)){
                JSONObject hotjson = new JSONObject();
                String uuid = UuidUtils.getUUID("hotSearch");
                hotjson.put("id", uuid);
                hotjson.put("searchContent", more);
                hotjson.put("date", new Date());
                restTemplate.postForEntity(filmServiceUrl+ "/hotSearch/add",hotjson, String.class).getBody();
            }

            String resultJson = restTemplate.postForEntity(filmServiceUrl+ "/film/getAll",json, String.class).getBody();
            // String resultJson = HttpUtil.post(baseApiUrl+"/film/getAll",json.toJSONString());
            resultMap = JSON.parseObject(resultJson);
            // 总页数
            int count = (int) resultMap.get("total");
            resultMap.put("totalPage", (count % pageSize == 0) ? count / pageSize : count / pageSize + 1);
            // 当前页数
            resultMap.put("curPage", pageNo);
            // 每页记录数
            resultMap.put("pageLine", pageSize);
        } catch (Exception e) {
            resultMap.put("msg", e.getMessage());
            resultMap.put("code", "1");
            logger.error(e.getMessage());
        }
        return resultMap;
    }

    /**
     * 分页查询
     *
     * @param request
     * @return
     */
    @RequestMapping("/hotList")
    @ResponseBody
    @HystrixCommand(fallbackMethod = "fallback")
    public Map<String, Object> hotList(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
            // 执行查询
            JSONObject json = new JSONObject();
            json.put("start",0);
            json.put("number",7);
            String resultJson = restTemplate.postForEntity(filmServiceUrl+ "/hotSearch/getAll",json, String.class).getBody();
            // String resultJson = HttpUtil.post(baseApiUrl+"/film/getAll",json.toJSONString());
            resultMap = JSON.parseObject(resultJson);
            // 总页数
            int count = (int) resultMap.get("total");
            resultMap.put("totalPage", (count % 7 == 0) ? count / 7 : count / 7 + 1);
            // 当前页数
            resultMap.put("curPage", 1);
            // 每页记录数
            resultMap.put("pageLine", 7);
        } catch (Exception e) {
            resultMap.put("msg", e.getMessage());
            resultMap.put("code", "1");
            logger.error(e.getMessage());
        }
        return resultMap;
    }


    /**
     * 点评跳转
     * @return
     */
    @RequestMapping("/review")
    public String index(HttpServletRequest request) {

        return "review";
    }

    /**
     * 添加点评
     * @param request
     * @return
     */
    @RequestMapping("/addReview")
    @ResponseBody
    @HystrixCommand(fallbackMethod = "fallback")
    public Map<String, Object> addReview(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        JSONObject json = new JSONObject();
        String uuid = UuidUtils.getUUID("filmReview");
        json.put("id", uuid);
        json.put("filmName", request.getParameter("filmName"));
        json.put("filmId", request.getParameter("filmId"));
        json.put("userAccount", request.getParameter("userAccount"));
        json.put("date", new Date());
        json.put("level", request.getParameter("level"));
        json.put("content", request.getParameter("content"));

        try {
            String resultJson = restTemplate.postForEntity(filmServiceUrl+ "/filmReview/add",json, String.class).getBody();
            //String resultJson = HttpUtil.post(baseApiUrl + "/filmReview/add",json.toJSONString());
            resultMap = JSON.parseObject(resultJson);
        } catch (Exception e) {
            resultMap.put("msg", e.getMessage());
            resultMap.put("code", "1");
            logger.error(e.getMessage());
        }
        return resultMap;
    }


    /**
     *短路由的回调方法
     * @param request
     * @return
     */
    public Map<String, Object> fallback(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("msg", request.getRequestURI()+" 发生断路由");
        resultMap.put("code", "1");
        logger.error("请求："+request.getRequestURI()+" 发生断路由");
        return resultMap;
    }
}