package com.ljw.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ljw.utils.Md5SaltUtils;
import com.ljw.utils.UuidUtils;
import com.ljw.utils.VerifyCodeUtils;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
public class LoginController {

	private Logger logger = Logger.getLogger(LoginController.class);

	@Autowired
	RestTemplate restTemplate;

	@Value("${user.role.authority.url}")
	private String authorityUrl;


	/**
	 * 首页跳转
	 * @return
	 */
	@RequestMapping("/")
	public String index(HttpServletRequest request) {

		return "redirect:index";
	}

	@RequestMapping("/index")
	public String home(HttpServletRequest request) {

		return "index";
	}


	@RequestMapping("/forgetPwd1")
	public String forgetPsw1(HttpServletRequest request) {
		return "forgetPwd1";
	}
	@RequestMapping("/forgetPwd2")
	public String forgetPsw2(HttpServletRequest request) {
		request.setAttribute("uid",request.getParameter("uid"));
		return "forgetPwd2";
	}
	@RequestMapping("/forgetPwd3")
	public String forgetPsw3(HttpServletRequest request) {
		return "forgetPwd3";
	}


	/**
	* 检查账号和邮箱验证
	* @param request
	* @return
	*/
	@RequestMapping("/checkAuth")
	@ResponseBody
	@HystrixCommand(fallbackMethod = "fallback")
	public Map<String, Object> checkAuth(HttpServletRequest request) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String email = request.getParameter("email");
		String account = request.getParameter("account");
		String code = request.getParameter("code");
		String emailCode = request.getSession().getAttribute("emailCode")+"";
		try {
			// 执行查询
			if (code.equalsIgnoreCase(emailCode)){
				JSONObject json = new JSONObject();
				json.put("start",0);
				json.put("number",1);
				json.put("account",account);
				String resultJson = restTemplate.postForEntity(authorityUrl+ "/user/getAll",json, String.class).getBody();
				resultMap = JSON.parseObject(resultJson);

				List list = ((List<Map<String,Object>>)resultMap.get("data"));

				if (list.size()>0 ){
					 Map<String,Object> map = (Map<String, Object>) list.get(0);
					 if (email.equals(map.get("email"))){
						 resultMap.put("uid", map.get("id"));
						 resultMap.put("code", 0);
						 resultMap.put("msg","验证成功");
				     }else {
						 resultMap.put("code", 1);
						 resultMap.put("msg","账号和邮箱不匹配");
					 }

				}else {
					resultMap.put("code", 1);
					resultMap.put("msg","账号不存在");
				}
			}else {
				resultMap.put("code", 1);
				resultMap.put("msg","验证码错误");
			}

		} catch (Exception e) {
			resultMap.put("code", 1);
			resultMap.put("msg", e.getMessage());
			logger.error(e.getMessage());
		}
		return resultMap;
	}

	/**
	 * 发送邮箱验证码
	 * @param request
	 * @return
	 */
	@RequestMapping("/sentEmail")
	@ResponseBody
	@HystrixCommand(fallbackMethod = "sentEmail")
	public Map<String, Object> sentEmail(HttpServletRequest request) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
			// 执行查询
			String emailCode = VerifyCodeUtils.generateVerifyCode(4).toLowerCase();
			//存入会话session
			HttpSession session = request.getSession(true);
			//删除以前的
			session.removeAttribute("emailCode");
			session.setAttribute("emailCode", emailCode);

			JSONObject json = new JSONObject();
			json.put("to",request.getParameter("to"));
			json.put("text","验证码："+emailCode);
			String resultJson = restTemplate.postForEntity(authorityUrl+ "/email/sent",json, String.class).getBody();
			resultMap = JSON.parseObject(resultJson);

		} catch (Exception e) {
			resultMap.put("code", 1);
			resultMap.put("msg", e.getMessage());
			logger.error(e.getMessage());
		}
		return resultMap;
	}


	/**
	 * 检查账号的唯一性
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping("/checkAccount")
	@ResponseBody
	@HystrixCommand(fallbackMethod = "fallback")
	public Map<String, Object> checkAccount(HttpServletRequest request) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
			// 执行查询
			JSONObject json = new JSONObject();
			json.put("start",0);
			json.put("number",1);
			json.put("account",request.getParameter("account"));
			String resultJson = restTemplate.postForEntity(authorityUrl+ "/user/getAll",json, String.class).getBody();
			int total = (int) JSON.parseObject(resultJson).get("total");

			if ( total>0){
				resultMap.put("valid", false);
			}else {
				resultMap.put("valid", true);
			}

		} catch (Exception e) {
			resultMap.put("valid", false);
			logger.error(e.getMessage());
		}
		return resultMap;
	}

	/**
	 * 登陆
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping("/login")
	@ResponseBody
	@HystrixCommand(fallbackMethod = "fallback")
	public Map<String, Object> login(HttpServletRequest request) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
			// 执行查询
			String loginAccount = request.getParameter("loginAccount");
			String loginPassword = request.getParameter("loginPassword");
			String resultJson = restTemplate.getForEntity(authorityUrl+ "/user/validateLogon/"+loginAccount, String.class).getBody();
			resultJson = resultJson.substring(14,resultJson.length()-1);
			JSONObject json = (JSONObject) JSON.parse(resultJson);
			String password = json.get("password")+"";
			String id =  json.get("id")+"";
			boolean flag = Md5SaltUtils.validPassword(loginPassword,password);
            if (flag){
				resultMap.put("code", "0");
				resultMap.put("msg", "登陆成功");
				resultJson = restTemplate.getForEntity(authorityUrl+ "/user/getById/"+id, String.class).getBody();
				Map<String,Object> map = (Map<String,Object>) ((JSONObject) JSON.parse(resultJson)).get("data");
				request.getSession().setAttribute("currentUser",map);
				resultMap.put("data",map);
			}else {
				resultMap.put("code", "1");
				resultMap.put("msg", "账号或密码不正确");
			}

		} catch (Exception e) {
			resultMap.put("code", 1);
			resultMap.put("msg", e.getMessage());
			logger.error(e.getMessage());
		}
		return resultMap;
	}

	/**
	 * 注销
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping("/logout")
	@ResponseBody
	@HystrixCommand(fallbackMethod = "fallback")
	public Map<String, Object> logout(HttpServletRequest request) {
		Map<String,Object> resultMap = new HashMap<String,Object>();
		request.getSession().removeAttribute("currentUser");
		resultMap.put("code",0);
		resultMap.put("msg","注销成功");
		return resultMap;
	}

	/**
	 * 注册
	 * @param request
	 * @return
	 */
	@RequestMapping("/register")
	@ResponseBody
	@HystrixCommand(fallbackMethod = "fallback")
	public Map<String, Object> register(HttpServletRequest request) {

		Map<String, Object> resultMap = new HashMap<String, Object>();

		HttpSession session = request.getSession(true);
		String verCode = (String)session.getAttribute("CAPTCHA");
		if (!verCode.equals(request.getParameter("captcha").toLowerCase())){
			resultMap.put("msg","验证码不正确");
			resultMap.put("code","1");
			return resultMap;
		}

		JSONObject json = new JSONObject();
		String uuid = UuidUtils.getUUID("user");
		json.put("id", uuid);
		json.put("account", request.getParameter("account"));
		json.put("email", request.getParameter("email"));
		json.put("tel", request.getParameter("tel"));
		json.put("sex", request.getParameter("sex"));
		String passwd ="";
		try {
			passwd = Md5SaltUtils.getEncryptedPwd(request.getParameter("password"));
		}catch (Exception e) {
			logger.error(e.getMessage());
		}
		json.put("password",passwd);

		try {
			String resultJson = restTemplate.postForEntity(authorityUrl+ "/user/add",json, String.class).getBody();
			//String resultJson = HttpUtil.post(baseApiUrl + "/user/deleteByIds",json.toJSONString());
			resultMap = JSON.parseObject(resultJson);
		} catch (Exception e) {
			resultMap.put("msg", e.getMessage());
			resultMap.put("code", "1");
			logger.error(e.getMessage());
		}
		return resultMap;
	}

	/**
	 * 修改密码
	 * @param request
	 * @return
	 */
	@RequestMapping("/changeNewPwd")
	@ResponseBody
	public Map<String, Object> changeNewPwd(HttpServletRequest request) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String id =  request.getParameter("uid");
		String newPassword = request.getParameter("password");
		JSONObject json = new JSONObject();
		try {
				json.put("id",id);
				json.put("password",Md5SaltUtils.getEncryptedPwd(newPassword));
				String resultJson = restTemplate.postForEntity(authorityUrl+ "/user/update",json, String.class).getBody();
				resultMap = JSON.parseObject(resultJson);
				return resultMap;

		} catch (Exception e) {
			resultMap.put("msg", "操作失败");
			resultMap.put("code", "1");
		}
		return resultMap;
	}

	/**
	 * 验证码
	 * @param request
	 * @param response
     */
	@RequestMapping("/authImage")
	public void authImage(HttpServletRequest request,HttpServletResponse response){
		response.setHeader("Pragma", "No-cache"); 
        response.setHeader("Cache-Control", "no-cache"); 
        response.setDateHeader("Expires", 0); 
        response.setContentType("image/jpeg");
        //生成随机字串 
        String verifyCode = VerifyCodeUtils.generateVerifyCode(4);
        //存入会话session 
        HttpSession session = request.getSession(true); 
        //删除以前的
        session.removeAttribute("CAPTCHA");
        session.setAttribute("CAPTCHA", verifyCode.toLowerCase());
        //生成图片 
        int w = 200, h = 55;
		try {
			VerifyCodeUtils.outputImage(w, h, response.getOutputStream(), verifyCode);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 *短路由的回调方法
	 * @param request
	 * @return
	 */
	public Map<String, Object> fallback(HttpServletRequest request) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("msg", request.getRequestURI()+" 发生断路由");
		resultMap.put("code", "1");
		logger.error("请求："+request.getRequestURI()+" 发生断路由");
		return resultMap;
	}

}
