package com.ljw.sso.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ljw.sso.info.UserInfo;
import com.ljw.utils.Md5SaltUtils;
import com.ljw.utils.VerifyCodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.cas.authentication.CasAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


@Controller
public class LoginController {

	@Autowired
	RestTemplate restTemplate;

	@Value("${user.role.authority.url}")
	private String authorityUrl;

	/** spring security 权限
	 *  这里注意的是admin只是权限编码，
	 */
	@PreAuthorize("hasAnyAuthority('admin','')")//必须要有admin权限的才能访问
	@RequestMapping(value = "/")
	public String index(HttpServletRequest request ,Map<String, Object> model) {
		SecurityContext sc = (SecurityContext) request.getSession().getAttribute("SPRING_SECURITY_CONTEXT");
		CasAuthenticationToken cat = (CasAuthenticationToken) sc.getAuthentication();
		request.getSession().setAttribute("CURRENT_USER_INFO",cat.getUserDetails());
		model.put("currentUserInfo",request.getSession().getAttribute("CURRENT_USER_INFO"));
		return "index";
	}

	@RequestMapping(value = "/myApps")
	public String myApps(HttpServletRequest request, HttpServletResponse response) {
		UserInfo userInfo  = (UserInfo) request.getSession().getAttribute("CURRENT_USER_INFO");
		JSONObject json = new JSONObject();
		json.put("ownerIds", userInfo.getUsername());
		String resultJson = restTemplate.postForEntity(authorityUrl+ "/application/getAll",json, String.class).getBody();
		Map<String, Object> resultMap = JSON.parseObject(resultJson);
		request.setAttribute("dataList",resultMap.get("data"));
		return "apps";
	}

	@RequestMapping(value = "/toChangePassword")
	public String toChangePassword(HttpServletRequest request, HttpServletResponse response) {
		return "changepsw";
	}


	@RequestMapping(value = "/toRegister")
	public String toRegister() {
		return "register";
	}

	@ResponseBody
	@RequestMapping("/register")
	public Map register(HttpServletRequest request){
		Map<String ,Object> result = new HashMap<String,Object>();
		HttpSession session = request.getSession(true);
		String verCode = (String)session.getAttribute("CAPTCHA");
		if(!verCode.equalsIgnoreCase("")){
			result.put("msg", "验证码输入错误");
		}else {
			result.put("msg", "用户已经存在");
		}
		return result;
	}


	/**
	 * 验证码
	 * @param request
	 * @param response
     */
	@RequestMapping("/authImage")
	public void authImage(HttpServletRequest request,HttpServletResponse response){
		response.setHeader("Pragma", "No-cache"); 
        response.setHeader("Cache-Control", "no-cache"); 
        response.setDateHeader("Expires", 0); 
        response.setContentType("image/jpeg");
        //生成随机字串 
        String verifyCode = VerifyCodeUtils.generateVerifyCode(4);
        //存入会话session 
        HttpSession session = request.getSession(true); 
        //删除以前的
        session.removeAttribute("CAPTCHA");
        session.setAttribute("CAPTCHA", verifyCode.toLowerCase());
        //生成图片 
        int w = 200, h = 55;
		try {
			VerifyCodeUtils.outputImage(w, h, response.getOutputStream(), verifyCode);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 修改密码
	 * @param request
	 * @return
	 */
	@RequestMapping("/changePassword")
	@ResponseBody
	public Map<String, Object> changePassword(HttpServletRequest request) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String password =  request.getParameter("password");
		String newPassword = request.getParameter("newpassword");
		UserInfo userInfo  = (UserInfo) request.getSession().getAttribute("CURRENT_USER_INFO");
		JSONObject json = new JSONObject();
		try {
			if(Md5SaltUtils.validPassword(password,userInfo.getPassword())){
				json.put("id", userInfo.getId());
				json.put("password",Md5SaltUtils.getEncryptedPwd(newPassword));
				String resultJson = restTemplate.postForEntity(authorityUrl+ "/user/update",json, String.class).getBody();
				resultMap = JSON.parseObject(resultJson);
				return resultMap;
			}else{
				resultMap.put("msg", "原密码不正确");
				resultMap.put("code", "1");
				return resultMap;
			}
		} catch (Exception e) {
			resultMap.put("msg", "操作失败");
			resultMap.put("code", "1");
		}
		return resultMap;
	}

}
