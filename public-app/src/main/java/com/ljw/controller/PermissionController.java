package com.ljw.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 钜文 on 2017/4/18.
 */
@Controller
@RequestMapping("/permission")
public class PermissionController {

        private Logger logger = Logger.getLogger(PermissionController.class);

        @Autowired
        RestTemplate restTemplate;

        @Autowired
        UserRoleController userRoleController;

        @Value("${base.api.url}")
        private String baseApiUrl;


        /**
         * 列表跳转
         * @return
         */
        @RequestMapping("/toList")
        public String toList() {
            return "permission/list";
        }

        /**
         * 添加跳转
         * @return
         */
        @RequestMapping("/toAdd")
        public String toAdd(HttpServletRequest request) {
            List<Map<String, Object>> list = userRoleController.allRoleId(request);
            List<String> nameList = new ArrayList<String>();
            for (Map<String, Object> map : list){
                nameList.add(map.get("roleId")+"");
            }
            request.setAttribute("list",nameList);
            return "permission/add";
        }

        /**
         * 修改跳转
         * @return
         */
        @RequestMapping("/toEdit")
        public String toEdit(HttpServletRequest request) {
            Map<String, Object> map = getById(request);
            List<Map<String, Object>> list = userRoleController.allRoleId(request);
            List<String> nameList = new ArrayList<String>();
            for (Map<String, Object> amap : list){
                nameList.add(amap.get("roleId")+"");
            }
            request.setAttribute("list",nameList);
            request.setAttribute("data",map.get("data"));
            return "permission/edit";
        }

    /**
     * 检查key的唯一性
     *
     * @param request
     * @return
     */
    @RequestMapping("/checkKey")
    @ResponseBody
    @HystrixCommand(fallbackMethod = "fallback")
    public Map<String, Object> checkKey(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();

        try {
            // 执行查询
            JSONObject json = new JSONObject();
            json.put("start",0);
            json.put("number",1);
            json.put("perKey",request.getParameter("perKey"));
            String resultJson = restTemplate.postForEntity(baseApiUrl+ "/permission/getAll",json, String.class).getBody();
            int total = (int) JSON.parseObject(resultJson).get("total");
                if ( total>0){
                    resultMap.put("valid", false);
                }else {
                    resultMap.put("valid", true);
                }
        } catch (Exception e) {
            resultMap.put("valid", false);
            logger.error(e.getMessage());
        }
        return resultMap;
    }
        /**
         * 分页查询
         *
         * @param request
         * @return
         */
        @RequestMapping("/list")
        @ResponseBody
        @HystrixCommand(fallbackMethod = "fallback")
        public Map<String, Object> list(HttpServletRequest request) {
            Map<String, Object> resultMap = new HashMap<String, Object>();
            try {
                // 执行查询
                JSONObject json = new JSONObject();
                Integer pageNo = Integer.parseInt(request.getParameter("pageNo"));
                Integer pageSize = Integer.parseInt(request.getParameter("pageSize"));
                json.put("start",(pageNo-1)*pageSize);
                json.put("number",pageSize);
                json.put("permissionname",request.getParameter("permissionname"));
                String resultJson = restTemplate.postForEntity(baseApiUrl+ "/permission/getAll",json, String.class).getBody();
                // String resultJson = HttpUtil.post(baseApiUrl+"/userRole/getAll",json.toJSONString());
                resultMap = JSON.parseObject(resultJson);
                // 总页数
                int count = (int) resultMap.get("total");
                resultMap.put("totalPage", (count % pageSize == 0) ? count / pageSize : count / pageSize + 1);
                // 当前页数
                resultMap.put("curPage", pageNo);
                // 每页记录数
                resultMap.put("pageLine", pageSize);
            } catch (Exception e) {
                resultMap.put("msg", e.getMessage());
                resultMap.put("code", "1");
                logger.error(e.getMessage());
            }
            return resultMap;
        }


        /**
         * 根据ids批量删除
         * @param request
         * @return
         */
        @RequestMapping("/deleteByIds")
        @ResponseBody
        @HystrixCommand(fallbackMethod = "fallback")
        public Map<String, Object> deleteByIds(HttpServletRequest request) {
            Map<String, Object> resultMap = new HashMap<String, Object>();
            JSONObject json = new JSONObject();
            json.put("ids", request.getParameter("ids"));
            try {
                String resultJson = restTemplate.postForEntity(baseApiUrl+ "/permission/deleteByIds",json, String.class).getBody();
                //String resultJson = HttpUtil.post(baseApiUrl + "/userRole/deleteByIds",json.toJSONString());
                resultMap = JSON.parseObject(resultJson);
            } catch (Exception e) {
                resultMap.put("msg", e.getMessage());
                resultMap.put("code", "1");
                logger.error(e.getMessage());
            }
            return resultMap;
        }

        /**
         * 根据id查找
         * @param request
         * @return
         */
        @RequestMapping("/getById")
        @ResponseBody
        @HystrixCommand(fallbackMethod = "fallback")
        public Map<String, Object> getById(HttpServletRequest request) {
            Map<String, Object> resultMap = new HashMap<String, Object>();
            try {
                String resultJson =restTemplate.getForEntity(baseApiUrl+"/permission/getById/"+request.getParameter("id"), String.class).getBody();
                //String resultJson = HttpUtil.post(baseApiUrl + "/userRole/deleteByIds",json.toJSONString());
                resultMap = JSON.parseObject(resultJson);
            } catch (Exception e) {
                resultMap.put("msg", e.getMessage());
                resultMap.put("code", "1");
                logger.error(e.getMessage());
            }
            return resultMap;
        }
        /**
         * 添加信息
         * @param request
         * @return
         */
        @RequestMapping("/add")
        @ResponseBody
        @HystrixCommand(fallbackMethod = "fallback")
        public Map<String, Object> add(HttpServletRequest request) {
            Map<String, Object> resultMap = new HashMap<String, Object>();
            JSONObject json = new JSONObject();
            json.put("roleId", request.getParameter("roleId"));
            json.put("permissionname", request.getParameter("permissionname"));
            json.put("perKey", request.getParameter("perKey"));
            try {
                String resultJson = restTemplate.postForEntity(baseApiUrl+ "/permission/add",json, String.class).getBody();
                //String resultJson = HttpUtil.post(baseApiUrl + "/userRole/deleteByIds",json.toJSONString());
                resultMap = JSON.parseObject(resultJson);
                if (resultMap.get("msg").toString().contains("添加失败！KeyElement must not be null!")){
                    resultMap.put("code",0);
                }
            } catch (Exception e) {
                resultMap.put("msg", e.getMessage());
                resultMap.put("code", "1");
                logger.error(e.getMessage());
            }
            return resultMap;
        }

        /**
         * 修改信息
         * @param request
         * @return
         */
        @RequestMapping("/edit")
        @ResponseBody
        @HystrixCommand(fallbackMethod = "fallback")
        public Map<String, Object> edit(HttpServletRequest request) {
            Map<String, Object> resultMap = new HashMap<String, Object>();
            Map<String, Object> map = (Map<String, Object>) getById(request).get("data");
            JSONObject json = new JSONObject();
            json.put("id", request.getParameter("id"));
            json.put("roleId", request.getParameter("roleId"));
            json.put("permissionname", request.getParameter("permissionname"));
            json.put("perKey", request.getParameter("perKey"));
            try {
                String resultJson = restTemplate.postForEntity(baseApiUrl+ "/permission/update",json, String.class).getBody();
                //String resultJson = HttpUtil.post(baseApiUrl + "/userRole/deleteByIds",json.toJSONString());
                resultMap = JSON.parseObject(resultJson);
            } catch (Exception e) {
                resultMap.put("msg", e.getMessage());
                resultMap.put("code", "1");
                logger.error(e.getMessage());
            }
            return resultMap;
        }
        /**
         *短路由的回调方法
         * @param request
         * @return
         */
        public Map<String, Object> fallback(HttpServletRequest request) {
            Map<String, Object> resultMap = new HashMap<String, Object>();
            resultMap.put("msg", request.getRequestURI()+" 发生断路由");
            resultMap.put("code", "1");
            logger.warn("请求："+request.getRequestURI()+" 发生断路由");
            return resultMap;
        }


}
