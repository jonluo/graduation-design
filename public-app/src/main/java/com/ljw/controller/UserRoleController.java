package com.ljw.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 钜文 on 2017/4/18.
 */
@Controller
@RequestMapping("/userRole")
public class UserRoleController {

        private Logger logger = Logger.getLogger(UserRoleController.class);

        @Autowired
        RestTemplate restTemplate;

        @Autowired
        UserController userController;

        @Value("${base.api.url}")
        private String baseApiUrl;


        /**
         * 列表跳转
         * @return
         */
        @RequestMapping("/toList")
        public String toList() {
            return "userRole/list";
        }

        /**
         * 添加跳转
         * @return
         */
        @RequestMapping("/toAdd")
        public String toAdd(HttpServletRequest request) {
            List<Map<String, Object>> list = userController.allAccount(request);
            List<String> nameList = new ArrayList<String>();
            for (Map<String, Object> map : list){
                nameList.add(map.get("account")+"");
            }
            request.setAttribute("list",nameList);
            return "userRole/add";
        }

        /**
         * 修改跳转
         * @return
         */
        @RequestMapping("/toEdit")
        public String toEdit(HttpServletRequest request) {
            Map<String, Object> map = getById(request);
            List<Map<String, Object>> list = userController.allAccount(request);
            List<String> nameList = new ArrayList<String>();
            for (Map<String, Object> amap : list){
                nameList.add(amap.get("account")+"");
            }
            request.setAttribute("list",nameList);
            request.setAttribute("data",map.get("data"));

            return "userRole/edit";
        }


    /**
     *获取所有账号
     * @param request
     * @return
     */
    public List<Map<String, Object>> allRoleId(HttpServletRequest request) {
        List<Map<String, Object>> result = null;
        try {
            // 执行查询
            JSONObject json = new JSONObject();
            String resultJson = restTemplate.postForEntity(baseApiUrl+ "/userRole/getAll",json, String.class).getBody();
            result= (List<Map<String, Object>>) JSON.parseObject(resultJson).get("data");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return result;
    }

        /**
         * 分页查询
         *
         * @param request
         * @return
         */
        @RequestMapping("/list")
        @ResponseBody
        @HystrixCommand(fallbackMethod = "fallback")
        public Map<String, Object> list(HttpServletRequest request) {
            Map<String, Object> resultMap = new HashMap<String, Object>();
            try {
                // 执行查询
                JSONObject json = new JSONObject();
                Integer pageNo = Integer.parseInt(request.getParameter("pageNo"));
                Integer pageSize = Integer.parseInt(request.getParameter("pageSize"));
                json.put("start",(pageNo-1)*pageSize);
                json.put("number",pageSize);
                json.put("roleName",request.getParameter("roleName"));
                String resultJson = restTemplate.postForEntity(baseApiUrl+ "/userRole/getAll",json, String.class).getBody();
                // String resultJson = HttpUtil.post(baseApiUrl+"/userRole/getAll",json.toJSONString());
                resultMap = JSON.parseObject(resultJson);
                // 总页数
                int count = (int) resultMap.get("total");
                resultMap.put("totalPage", (count % pageSize == 0) ? count / pageSize : count / pageSize + 1);
                // 当前页数
                resultMap.put("curPage", pageNo);
                // 每页记录数
                resultMap.put("pageLine", pageSize);
            } catch (Exception e) {
                resultMap.put("msg", e.getMessage());
                resultMap.put("code", "1");
                logger.error(e.getMessage());
            }
            return resultMap;
        }


        /**
         * 根据ids批量删除
         * @param request
         * @return
         */
        @RequestMapping("/deleteByIds")
        @ResponseBody
        @HystrixCommand(fallbackMethod = "fallback")
        public Map<String, Object> deleteByIds(HttpServletRequest request) {
            Map<String, Object> resultMap = new HashMap<String, Object>();
            JSONObject json = new JSONObject();
            json.put("ids", request.getParameter("ids"));
            try {
                String resultJson = restTemplate.postForEntity(baseApiUrl+ "/userRole/deleteByIds",json, String.class).getBody();
                //String resultJson = HttpUtil.post(baseApiUrl + "/userRole/deleteByIds",json.toJSONString());
                resultMap = JSON.parseObject(resultJson);
            } catch (Exception e) {
                resultMap.put("msg", e.getMessage());
                resultMap.put("code", "1");
                logger.error(e.getMessage());
            }
            return resultMap;
        }

        /**
         * 根据id查找
         * @param request
         * @return
         */
        @RequestMapping("/getById")
        @ResponseBody
        @HystrixCommand(fallbackMethod = "fallback")
        public Map<String, Object> getById(HttpServletRequest request) {
            Map<String, Object> resultMap = new HashMap<String, Object>();
            try {
                String resultJson =restTemplate.getForEntity(baseApiUrl+"/userRole/getById/"+request.getParameter("id"), String.class).getBody();
                //String resultJson = HttpUtil.post(baseApiUrl + "/userRole/deleteByIds",json.toJSONString());
                resultMap = JSON.parseObject(resultJson);
            } catch (Exception e) {
                resultMap.put("msg", e.getMessage());
                resultMap.put("code", "1");
                logger.error(e.getMessage());
            }
            return resultMap;
        }
        /**
         * 添加信息
         * @param request
         * @return
         */
        @RequestMapping("/add")
        @ResponseBody
        @HystrixCommand(fallbackMethod = "fallback")
        public Map<String, Object> add(HttpServletRequest request) {
            Map<String, Object> resultMap = new HashMap<String, Object>();
            JSONObject json = new JSONObject();
            String uuid = "userRole"+(int)(Math.random()*1000000);
            json.put("roleId", uuid);
            json.put("roleName", request.getParameter("roleName"));
            json.put("userAccount", request.getParameter("account"));
            try {
                String resultJson = restTemplate.postForEntity(baseApiUrl+ "/userRole/add",json, String.class).getBody();
                //String resultJson = HttpUtil.post(baseApiUrl + "/userRole/deleteByIds",json.toJSONString());
                resultMap = JSON.parseObject(resultJson);
                if (resultMap.get("msg").toString().contains("添加失败！KeyElement must not be null!")){
                    resultMap.put("code",0);
                }
            } catch (Exception e) {
                resultMap.put("msg", e.getMessage());
                resultMap.put("code", "1");
                logger.error(e.getMessage());
            }
            return resultMap;
        }

        /**
         * 修改信息
         * @param request
         * @return
         */
        @RequestMapping("/edit")
        @ResponseBody
        @HystrixCommand(fallbackMethod = "fallback")
        public Map<String, Object> edit(HttpServletRequest request) {
            Map<String, Object> resultMap = new HashMap<String, Object>();
            Map<String, Object> map = (Map<String, Object>) getById(request).get("data");
            JSONObject json = new JSONObject();
            json.put("id", request.getParameter("id"));
            json.put("roleId", request.getParameter("roleId"));
            json.put("roleName", request.getParameter("roleName"));
            json.put("userAccount", request.getParameter("account"));

            try {
                String resultJson = restTemplate.postForEntity(baseApiUrl+ "/userRole/update",json, String.class).getBody();
                //String resultJson = HttpUtil.post(baseApiUrl + "/userRole/deleteByIds",json.toJSONString());
                resultMap = JSON.parseObject(resultJson);
            } catch (Exception e) {
                resultMap.put("msg", e.getMessage());
                resultMap.put("code", "1");
                logger.error(e.getMessage());
            }
            return resultMap;
        }
        /**
         *短路由的回调方法
         * @param request
         * @return
         */
        public Map<String, Object> fallback(HttpServletRequest request) {
            Map<String, Object> resultMap = new HashMap<String, Object>();
            resultMap.put("msg", request.getRequestURI()+" 发生断路由");
            resultMap.put("code", "1");
            logger.warn("请求："+request.getRequestURI()+" 发生断路由");
            return resultMap;
        }


}
