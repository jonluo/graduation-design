package com.ljw.utils;


import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Created by 钜文 on 2017/3/12.
 */
public class HttpUtil {

         /**
         * 发送http请求
          * @param url 地址
          * @param jsonData json字符串数据
          * @param method get 或 post
         * */
    private static String sendInfo(String url, String jsonData ,String method) {
        CloseableHttpClient client = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        // 响应json内容
        String responseContent = null;
        try {
            HttpPost post = null;
            if ("post".equals(method.toLowerCase())){
                post = new HttpPost(url);
                // 构造请求数据
                StringEntity myEntity = new StringEntity(jsonData, ContentType.APPLICATION_JSON);
                // 设置请求体
                post.setEntity(myEntity);
                response = client.execute(post);
            }
            HttpGet get = null;
            if ("get".equals(method.toLowerCase())){
                // 创建HttpGet实例
                get = new HttpGet(url);
                response = client.execute(get);
            }

            if (response.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = response.getEntity();
                responseContent = EntityUtils.toString(entity, "UTF-8");
            }
        }catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (response != null)response.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (client != null)client.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return responseContent;
    }

    /**
     * get 请求
     * @param url
     * @return
     */
    public static String get(String url) {
        return  sendInfo(url,null,"get");
    }

    /**
     * post 请求
     * @param url
     * @param jsonData
     * @return
     */
    public static String post(String url, String jsonData) {
        return  sendInfo(url,jsonData,"post");
    }

   /* public static void main(String[] args) {
        String json = "{}";
        // String result = sendInfo("http://localhost:8888/user/getById/abc111",null,"get");
        String result = sendInfo("http://localhost:8888/user/getAll",json,"POST");
        System.out.println(result);
    }*/

}
