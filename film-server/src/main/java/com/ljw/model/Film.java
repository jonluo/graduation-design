package com.ljw.model;

import java.io.Serializable;
import java.util.Date;

public class Film  implements Serializable{
    /**
    * id
    */
    private String id;

    /**
    * 电影名
    */
    private String name;

    /**
    * 图片路径
    */
    private String pathName;

    /**
    * 导演
    */
    private String director;

    /**
    * 编剧
    */
    private String screenwriter;

    /**
    * 上演时间
    */
    private Date uploadDate;

    /**
    * 主演
    */
    private String starring;

    /**
    * 类型
    */
    private String type;

    /**
    * 剧情简介
    */
    private String synopsis;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPathName() {
        return pathName;
    }

    public void setPathName(String pathName) {
        this.pathName = pathName;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getScreenwriter() {
        return screenwriter;
    }

    public void setScreenwriter(String screenwriter) {
        this.screenwriter = screenwriter;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getStarring() {
        return starring;
    }

    public void setStarring(String starring) {
        this.starring = starring;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }
}