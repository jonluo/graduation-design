package com.ljw.model;

import java.io.Serializable;
import java.util.Date;

public class FilmReview  implements Serializable{
    /**
    * id
    */
    private String id;

    /**
    * 电影名
    */
    private String filmName;

    /**
    * 电影id
    */
    private String filmId;

    /**
    * 用户账号
    */
    private String userAccount;

    /**
    * 评论时间
    */
    private Date date;

    /**
    * 类型
    */
    private Integer level;

    /**
    * 内容
    */
    private String content;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    public String getFilmId() {
        return filmId;
    }

    public void setFilmId(String filmId) {
        this.filmId = filmId;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}