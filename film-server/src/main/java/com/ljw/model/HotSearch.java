package com.ljw.model;

import java.io.Serializable;
import java.util.Date;

public class HotSearch implements Serializable{
    /**
    * id
    */
    private String id;

    /**
    * 搜索内容
    */
    private String searchContent;

    /**
    * 次数
    */
    private Integer times;

    /**
    * 搜索时间
    */
    private Date date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSearchContent() {
        return searchContent;
    }

    public void setSearchContent(String searchContent) {
        this.searchContent = searchContent;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}