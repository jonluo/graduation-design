package com.ljw.mapper;

import com.ljw.model.Film;
import com.ljw.model.FilmExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;
@Mapper
public interface FilmMapper {
    @Cacheable(value="filmCount",key="'filmCount'+#p0.toString()")
    int countByExample(FilmExample example);

    int deleteByExample(FilmExample example);
    @Caching(evict = { @CacheEvict(value={"filmList","filmCount"},allEntries=true),
            @CacheEvict(value="film",key="#p0")})
    int deleteByPrimaryKey(String id);

    int insert(Film record);
    @Caching(evict = { @CacheEvict(value={"filmList","filmCount"},allEntries=true),
            @CacheEvict(value="film",key="#p0.id")})
    int insertSelective(Film record);
    @Cacheable(value="filmList",key="'filmList'+#p0.toString()")
    List<Film> selectByExample(FilmExample example);
    @Cacheable(value="film",key="#p0")
    Film selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") Film record, @Param("example") FilmExample example);

    int updateByExample(@Param("record") Film record, @Param("example") FilmExample example);
    @Caching(evict = { @CacheEvict(value="filmList",allEntries=true),
            @CacheEvict(value="film",key="#p0.id")})
    int updateByPrimaryKeySelective(Film record);

    int updateByPrimaryKey(Film record);
}