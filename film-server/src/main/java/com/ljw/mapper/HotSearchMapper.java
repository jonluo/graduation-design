package com.ljw.mapper;

import com.ljw.model.HotSearch;
import com.ljw.model.HotSearchExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;
@Mapper
public interface HotSearchMapper {
    @Cacheable(value="hotSearchCount",key="'hotSearchCount'+#p0.toString()")
    int countByExample(HotSearchExample example);

    int deleteByExample(HotSearchExample example);
    @Caching(evict = { @CacheEvict(value={"hotSearchList","hotSearchCount"},allEntries=true),
            @CacheEvict(value="hotSearch",key="#p0")})
    int deleteByPrimaryKey(String id);

    int insert(HotSearch record);
    @Caching(evict = { @CacheEvict(value={"hotSearchList","hotSearchCount"},allEntries=true),
            @CacheEvict(value="hotSearch",key="#p0.id")})
    int insertSelective(HotSearch record);
    @Cacheable(value="hotSearchList",key="'hotSearchList'+#p0.toString()")
    List<HotSearch> selectByExample(HotSearchExample example);
    @Cacheable(value="hotSearch",key="#p0")
    HotSearch selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") HotSearch record, @Param("example") HotSearchExample example);

    int updateByExample(@Param("record") HotSearch record, @Param("example") HotSearchExample example);
    @Caching(evict = { @CacheEvict(value="hotSearchList",allEntries=true),
            @CacheEvict(value="hotSearch",key="#p0.id")})
    int updateByPrimaryKeySelective(HotSearch record);

    int updateByPrimaryKey(HotSearch record);
}