package com.ljw.mapper;

import com.ljw.model.FilmReview;
import com.ljw.model.FilmReviewExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;
import java.util.Map;

@Mapper
public interface FilmReviewMapper {
    @Cacheable(value="filmReviewCount",key="'filmReviewCount'+#p0.toString()")
    int countByExample(FilmReviewExample example);

    int deleteByExample(FilmReviewExample example);
    @Caching(evict = { @CacheEvict(value={"filmReviewList","filmReviewCount"},allEntries=true),
            @CacheEvict(value="filmReview",key="#p0")})
    int deleteByPrimaryKey(String id);

    int insert(FilmReview record);
    @Caching(evict = { @CacheEvict(value={"filmReviewList","filmReviewCount"},allEntries=true),
            @CacheEvict(value="filmReview",key="#p0.id")})
    int insertSelective(FilmReview record);
    @Cacheable(value="filmReviewList",key="'filmReviewList'+#p0.toString()")
    List<FilmReview> selectByExample(FilmReviewExample example);
    @Cacheable(value="filmReview",key="#p0")
    FilmReview selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") FilmReview record, @Param("example") FilmReviewExample example);

    int updateByExample(@Param("record") FilmReview record, @Param("example") FilmReviewExample example);
    @Caching(evict = { @CacheEvict(value="filmReviewList",allEntries=true),
            @CacheEvict(value="filmReview",key="#p0.id")})
    int updateByPrimaryKeySelective(FilmReview record);

    int updateByPrimaryKey(FilmReview record);

    List<Map<String,Object>> countGroupByLevel(@Param("filmId") String filmId);

}