package com.ljw.service;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ljw.mapper.FilmMapper;
import com.ljw.model.Film;
import com.ljw.model.FilmExample;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 钜文 on 2017/1/21.
 */
@RestController
@Transactional
@RequestMapping(value="/film")
public class FilmService {

    @Autowired
    private FilmMapper filmMapper;

    @ApiOperation(value="获取电影信息列表", notes="分页查询电影信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"start\":0,\"number\":\"10\",\"more\":\"12\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/getAll"}, method=RequestMethod.POST)
    public ResponseEntity getAll(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            Integer start = jsonObject.getInteger("start");
            Integer number = jsonObject.getInteger("number");
            String more = jsonObject.getString("more");

            FilmExample filmExample = new FilmExample();
            FilmExample.Criteria criteria1= filmExample.createCriteria();
            FilmExample.Criteria criteria2= filmExample.createCriteria();
            FilmExample.Criteria criteria3= filmExample.createCriteria();


            //分页
            filmExample.setOffset(start);
            filmExample.setLimit(number);
            //查询条件

            if (more != null && !"".equals(more)) {
                criteria1.andNameLike("%"+more+"%");
                criteria2.andTypeLike("%"+more+"%");
                criteria3.andDirectorLike("%"+more+"%");
                filmExample.or(criteria1);
                filmExample.or(criteria2);
                filmExample.or(criteria3);
            }

            List<Film> list  = filmMapper.selectByExample(filmExample);
            Integer total = filmMapper.countByExample(filmExample);

            map.put("code", "0");
            map.put("total", total);
            map.put("data", list);
            map.put("msg", "查询成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("total",0);
            map.put("data", "");
            map.put("msg", "查询失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }
    

    @ApiOperation(value="获取电影信息", notes="根据id查询电影信息",response = Map.class )
    @ApiImplicitParam(name = "id", value = "电影信息id",paramType = "path",required = true, dataType = "String")
    @RequestMapping(value={"/getById/{id}"}, method=RequestMethod.GET)
    public ResponseEntity getById(@PathVariable String id){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            Film film = filmMapper.selectByPrimaryKey(id);
            map.put("code", "0");
            map.put("data", film);
            map.put("msg", "查询成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("data", "");
            map.put("msg", "查询失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="添加电影信息", notes="添加电影信息信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"id\":\"id\",\"name\":\"1\",\"director\":\"1\",\"screenwriter\":\"1\",\"starring\":\"1\"" +
            ",\"pathName\":\"name\",\"uploadDate\":\"2017-1-1\",\"synopsis\":\"name\",\"type\":\"1\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/add"}, method=RequestMethod.POST)
    public ResponseEntity add(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{

            Film film = JSON.parseObject(jsonObject.toJSONString(), Film.class);
            Integer sum = filmMapper.insertSelective(film);
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "添加成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum",0);
            map.put("msg", "添加失败！"+e.getMessage());
        }
        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="修改电影信息", notes="根据id修改电影信息信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"id\":\"id\",\"name\":\"1\",\"director\":\"1\",\"screenwriter\":\"1\",\"starring\":\"1\"" +
            ",\"pathName\":\"name\",\"uploadDate\":\"2017-1-1\",\"synopsis\":\"name\",\"type\":\"1\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/update"}, method=RequestMethod.POST)
    public ResponseEntity update(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            Film film = JSON.parseObject(jsonObject.toJSONString(), Film.class);
            Integer sum = filmMapper.updateByPrimaryKeySelective(film);
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "修改成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum",0);
            map.put("msg", "修改失败！"+e.getMessage());
        }
        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="删除电影信息", notes="批量删除电影信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"ids\":\"id1,id2\"}多个id以逗号隔开", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/deleteByIds"}, method=RequestMethod.POST)
    public ResponseEntity deleteByIds(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            String idstring = jsonObject.getString("ids");
            String[] ids = idstring.split(",");
            Integer sum = 0;
            for (String id:ids){
                filmMapper.deleteByPrimaryKey(id);
                sum++;
            }
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "批量删除成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum", 0);
            map.put("msg", "批量删除失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }


}
