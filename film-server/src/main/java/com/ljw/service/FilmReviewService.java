package com.ljw.service;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ljw.mapper.FilmReviewMapper;
import com.ljw.model.FilmReview;
import com.ljw.model.FilmReviewExample;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 钜文 on 2017/1/21.
 */
@RestController
@Transactional
@RequestMapping(value="/filmReview")
public class FilmReviewService {

    @Autowired
    private FilmReviewMapper filmReviewMapper;

    @ApiOperation(value="获取影评信息列表", notes="分页查询影评信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"start\":0,\"number\":\"10\",\"filmId\":\"12\",\"userName\":\"12\",\"filmName\":\"12\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/getAll"}, method=RequestMethod.POST)
    public ResponseEntity getAll(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            Integer start = jsonObject.getInteger("start");
            Integer number = jsonObject.getInteger("number");
            String filmId = jsonObject.getString("filmId");
            String filmName = jsonObject.getString("filmName");
            String userName = jsonObject.getString("userName");


            FilmReviewExample filmReviewExample = new FilmReviewExample();
            FilmReviewExample.Criteria criteria= filmReviewExample.createCriteria();


            //分页
            filmReviewExample.setOffset(start);
            filmReviewExample.setLimit(number);
            filmReviewExample.setOrderByClause(" date desc");
            //查询条件

            if (filmName != null && !"".equals(filmName)) {
                criteria.andFilmNameLike("%"+filmName+"%");
            }
            if (userName != null && !"".equals(userName)) {
                criteria.andUserAccountLike("%"+userName+"%");
            }
            if (filmId != null && !"".equals(filmId)) {
                criteria.andFilmIdEqualTo(filmId);
            }

            List<FilmReview> list  = filmReviewMapper.selectByExample(filmReviewExample);
            Integer total = filmReviewMapper.countByExample(filmReviewExample);

            map.put("code", "0");
            map.put("total", total);
            map.put("data", list);
            map.put("msg", "查询成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("total",0);
            map.put("data", "");
            map.put("msg", "查询失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }
    

    @ApiOperation(value="获取影评信息", notes="根据id查询影评信息",response = Map.class )
    @ApiImplicitParam(name = "id", value = "影评信息id",paramType = "path",required = true, dataType = "String")
    @RequestMapping(value={"/getById/{id}"}, method=RequestMethod.GET)
    public ResponseEntity getById(@PathVariable String id){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            FilmReview filmReview = filmReviewMapper.selectByPrimaryKey(id);
            map.put("code", "0");
            map.put("data", filmReview);
            map.put("msg", "查询成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("data", "");
            map.put("msg", "查询失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="获取影评评分", notes="根据filmId查询影评评分",response = Map.class )
    @ApiImplicitParam(name = "filmId", value = "影评信息id",paramType = "path",required = true, dataType = "String")
    @RequestMapping(value={"/getByFilmId/{filmId}"}, method=RequestMethod.GET)
    public ResponseEntity getByFilmId(@PathVariable String filmId){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            List<Map<String, Object>> filmReviewList = filmReviewMapper.countGroupByLevel(filmId);
            map.put("code", "0");
            map.put("data", filmReviewList);
            map.put("msg", "查询成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("data", "");
            map.put("msg", "查询失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="添加影评信息", notes="添加影评信息信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"id\":\"id\",\"filmName\":\"1\",\"filmId\":\"1\",\"userAccount\":\"1\",\"content\":\"1\"" +
            ",\"date\":\"2017-1-1\",\"level\":1}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/add"}, method=RequestMethod.POST)
    public ResponseEntity add(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{

            FilmReview filmReview = JSON.parseObject(jsonObject.toJSONString(), FilmReview.class);
            Integer sum = filmReviewMapper.insertSelective(filmReview);
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "添加成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum",0);
            map.put("msg", "添加失败！"+e.getMessage());
        }
        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="修改影评信息", notes="根据id修改影评信息信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"id\":\"id\",\"filmName\":\"1\",\"filmId\":\"1\",\"userAccount\":\"1\",\"content\":\"1\"" +
            ",\"date\":\"2017-1-1\",\"level\":1}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/update"}, method=RequestMethod.POST)
    public ResponseEntity update(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            FilmReview filmReview = JSON.parseObject(jsonObject.toJSONString(), FilmReview.class);
            Integer sum = filmReviewMapper.updateByPrimaryKeySelective(filmReview);
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "修改成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum",0);
            map.put("msg", "修改失败！"+e.getMessage());
        }
        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="删除影评信息", notes="批量删除影评信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"ids\":\"id1,id2\"}多个id以逗号隔开", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/deleteByIds"}, method=RequestMethod.POST)
    public ResponseEntity deleteByIds(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            String idstring = jsonObject.getString("ids");
            String[] ids = idstring.split(",");
            Integer sum = 0;
            for (String id:ids){
                filmReviewMapper.deleteByPrimaryKey(id);
                sum++;
            }
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "批量删除成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum", 0);
            map.put("msg", "批量删除失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }


}
