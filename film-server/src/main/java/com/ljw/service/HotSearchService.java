package com.ljw.service;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ljw.mapper.HotSearchMapper;
import com.ljw.model.HotSearch;
import com.ljw.model.HotSearchExample;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 钜文 on 2017/1/21.
 */
@RestController
@Transactional
@RequestMapping(value="/hotSearch")
public class HotSearchService {

    @Autowired
    private HotSearchMapper hotSearchMapper;

    @ApiOperation(value="获取搜索信息列表", notes="分页查询搜索信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"start\":0,\"number\":\"10\",\"more\":\"12\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/getAll"}, method=RequestMethod.POST)
    public ResponseEntity getAll(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            Integer start = jsonObject.getInteger("start");
            Integer number = jsonObject.getInteger("number");
            String more = jsonObject.getString("more");

            HotSearchExample hotSearchExample = new HotSearchExample();
            HotSearchExample.Criteria criteria= hotSearchExample.createCriteria();


            //分页
            hotSearchExample.setOffset(start);
            hotSearchExample.setLimit(number);
            hotSearchExample.setOrderByClause(" times desc ");
            //查询条件

            if (more != null && !"".equals(more)) {
                criteria.andSearchContentLike("%"+more+"%");
            }

            List<HotSearch> list  = hotSearchMapper.selectByExample(hotSearchExample);
            Integer total = hotSearchMapper.countByExample(hotSearchExample);

            map.put("code", "0");
            map.put("total", total);
            map.put("data", list);
            map.put("msg", "查询成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("total",0);
            map.put("data", "");
            map.put("msg", "查询失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }
    

    @ApiOperation(value="获取搜索信息", notes="根据id查询搜索信息",response = Map.class )
    @ApiImplicitParam(name = "id", value = "搜索信息id",paramType = "path",required = true, dataType = "String")
    @RequestMapping(value={"/getById/{id}"}, method=RequestMethod.GET)
    public ResponseEntity getById(@PathVariable String id){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            HotSearch hotSearch = hotSearchMapper.selectByPrimaryKey(id);
            map.put("code", "0");
            map.put("data", hotSearch);
            map.put("msg", "查询成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("data", "");
            map.put("msg", "查询失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="添加搜索信息", notes="添加搜索信息信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"id\":\"id\",\"searchContent\":\"1\"" +
            ",\"date\":\"2017-1-1\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/add"}, method=RequestMethod.POST)
    public ResponseEntity add(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{

            HotSearch hotSearch = JSON.parseObject(jsonObject.toJSONString(), HotSearch.class);

            HotSearchExample hotSearchExample = new HotSearchExample();
            HotSearchExample.Criteria criteria= hotSearchExample.createCriteria();
            hotSearchExample.setOffset(0);
            hotSearchExample.setLimit(1);
            criteria.andSearchContentEqualTo(hotSearch.getSearchContent());
            List<HotSearch> list = hotSearchMapper.selectByExample(hotSearchExample);

            Integer sum = 0;

            if (list.size()>0){
                HotSearch tempHs = list.get(0);
                tempHs.setTimes(tempHs.getTimes()+1);
                sum = hotSearchMapper.updateByPrimaryKeySelective(tempHs);
            }else {
                hotSearch.setTimes(1);
                sum = hotSearchMapper.insertSelective(hotSearch);
            }


            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "添加成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum",0);
            map.put("msg", "添加失败！"+e.getMessage());
        }
        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="修改搜索信息", notes="根据id修改搜索信息信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"id\":\"id\",\"searchContent\":\"1\"" +
            ",\"date\":\"2017-1-1\",\"times\":\"1\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/update"}, method=RequestMethod.POST)
    public ResponseEntity update(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            HotSearch hotSearch = JSON.parseObject(jsonObject.toJSONString(), HotSearch.class);
            Integer sum = hotSearchMapper.updateByPrimaryKeySelective(hotSearch);
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "修改成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum",0);
            map.put("msg", "修改失败！"+e.getMessage());
        }
        return ResponseEntity.ok(map);
    }

    @ApiOperation(value="删除搜索信息", notes="批量删除搜索信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"ids\":\"id1,id2\"}多个id以逗号隔开", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/deleteByIds"}, method=RequestMethod.POST)
    public ResponseEntity deleteByIds(@RequestBody JSONObject jsonObject){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            String idstring = jsonObject.getString("ids");
            String[] ids = idstring.split(",");
            Integer sum = 0;
            for (String id:ids){
                hotSearchMapper.deleteByPrimaryKey(id);
                sum++;
            }
            map.put("code", "0");
            map.put("sum", sum);
            map.put("msg", "批量删除成功！");
        }catch (Exception e)
        {
            map.put("code", "1");
            map.put("sum", 0);
            map.put("msg", "批量删除失败！"+e.getMessage());
        }

        return ResponseEntity.ok(map);
    }


}
